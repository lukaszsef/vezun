<?php
/*
*   Template Name: Property Search Template
*/
get_header();


/* Theme Home Page Module */
$theme_search_module = get_option('theme_search_module');

switch($theme_search_module){
    case 'properties-map':
        get_template_part('banners/map_based_banner');
        break;

    default:
        get_template_part('banners/default_page_banner');
        break;
}


?>

    <div class="container"><div class="row"><div class="col-sm-12"><div class="top-search-grid"><?php get_template_part('template-parts/advance-search'); ?></div></div></div></div>
    <div class="container-fluid contents lisitng-grid-layout">
        <div class="row">
            <div class="col-sm-12 main-wrap">
                <div class="main">
                    <?php
                    /* Advance Search Form */
                    //get_template_part('template-parts/advance-search');
                    ?>

                    <section class="listing-layout property-grid">
                    		<div class="top-pagination clearfix"></div>
                    		
                    		                        <div class="search-header">
                    		                            <?php get_template_part('template-parts/sort-controls'); ?>
                    		                        </div>
                    		
                    		                        <div class="list-container clearfix">
                    		                            <div class="row"><?php
                    		                            		/* List of Properties on Homepage */
                    		                            		$number_of_properties = intval(get_option('theme_properties_on_search'));
                    		                            		$number_of_properties = 21;
                    		                            		if(!$number_of_properties){
                    		                            		    $number_of_properties = 25;
                    		                            		}
                    		                            		
                    		                            		$search_args = array(
                    		                            		    'post_type' => 'property',
                    		                            		    'posts_per_page' => $number_of_properties,
                    		                            		    'paged' => $paged
																);
																
																//sprawdzam czy podano dowolna dzielnice lub dowolne miasto
																if($_GET['location'][0] == "any"){
																	$_GET['location'] = "any";
																}
																if($_GET['dzielnica'][0] == "any"){
																	$_GET['dzielnica'] = "any";
																}														
                    		                            		
                    		                            		// Apply Search Filter
                    		                            		$search_args = apply_filters('real_homes_search_parameters',$search_args);
                    		                            		
                    		                            		$search_args = sort_properties($search_args);
                    		                            		
                    		                            		$search_query = new WP_Query( $search_args );
                    		                            		if ( $search_query->have_posts() ) :
                    		                            		    $post_count = 0;
                    		                            		    while ( $search_query->have_posts() ) :
                    		                            		        $search_query->the_post();
                    		                            		
                    		                            		        /* Display Property for Search Page */
                    		                            		        get_template_part('template-parts/property-for-grid');
                    		                            		
                    		                            		        // $post_count++;
                    		                            		        // if(0 == ($post_count % 2)){
                    		                            		            // echo '<div class="clearfix"></div>';
                    		                            		        // }
                    		                            		    endwhile;
                    		                            		    wp_reset_query();
                    		                            		else:
                    		                            		    ?><div class="alert-wrapper"><h4><?php _e('No Properties Found!', 'framework') ?></h4></div><?php
                    		                            		endif;
                    		                            		?></div>
                    		                        </div>
                    		
                    		                       <div id="pagination-bottom"> <?php theme_pagination( $search_query->max_num_pages); ?></div>

                    </section>

                </div><!-- End Main Content -->

            </div> <!-- End span12 -->

        </div><!-- End  row -->

    </div><!-- End content -->

<?php get_footer(); ?>