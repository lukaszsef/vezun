
"use strict";!function(e){e.attrFn=e.attrFn||{};var t="ontouchstart"in window,a={tap_pixel_range:5,swipe_h_threshold:50,swipe_v_threshold:50,taphold_threshold:750,doubletap_int:500,shake_threshold:15,touch_capable:t,orientation_support:"orientation"in window&&"onorientationchange"in window,startevent:t?"touchstart":"mousedown",endevent:t?"touchend":"mouseup",moveevent:t?"touchmove":"mousemove",tapevent:t?"tap":"click",scrollevent:t?"touchmove":"scroll",hold_timer:null,tap_timer:null};e.touch={},e.isTouchCapable=function(){return a.touch_capable},e.getStartEvent=function(){return a.startevent},e.getEndEvent=function(){return a.endevent},e.getMoveEvent=function(){return a.moveevent},e.getTapEvent=function(){return a.tapevent},e.getScrollEvent=function(){return a.scrollevent},e.touch.setSwipeThresholdX=function(e){if("number"!=typeof e)throw new Error("Threshold parameter must be a type of number");a.swipe_h_threshold=e},e.touch.setSwipeThresholdY=function(e){if("number"!=typeof e)throw new Error("Threshold parameter must be a type of number");a.swipe_v_threshold=e},e.touch.setDoubleTapInt=function(e){if("number"!=typeof e)throw new Error("Interval parameter must be a type of number");a.doubletap_int=e},e.touch.setTapHoldThreshold=function(e){if("number"!=typeof e)throw new Error("Threshold parameter must be a type of number");a.taphold_threshold=e},e.touch.setTapRange=function(e){if("number"!=typeof e)throw new Error("Ranger parameter must be a type of number");a.tap_pixel_range=threshold},e.each(["tapstart","tapend","tapmove","tap","singletap","doubletap","taphold","swipe","swipeup","swiperight","swipedown","swipeleft","swipeend","scrollstart","scrollend","orientationchange","tap2","taphold2"],function(t,a){e.fn[a]=function(e){return e?this.on(a,e):this.trigger(a)},e.attrFn[a]=!0}),e.event.special.tapstart={setup:function(){var t=this,o=e(t);o.on(a.startevent,function e(n){if(o.data("callee",e),n.which&&1!==n.which)return!1;var i=n.originalEvent,r={position:{x:a.touch_capable?i.touches[0].pageX:n.pageX,y:a.touch_capable?i.touches[0].pageY:n.pageY},offset:{x:a.touch_capable?Math.round(i.changedTouches[0].pageX-(o.offset()?o.offset().left:0)):Math.round(n.pageX-(o.offset()?o.offset().left:0)),y:a.touch_capable?Math.round(i.changedTouches[0].pageY-(o.offset()?o.offset().top:0)):Math.round(n.pageY-(o.offset()?o.offset().top:0))},time:Date.now(),target:n.target};return w(t,"tapstart",n,r),!0})},remove:function(){e(this).off(a.startevent,e(this).data.callee)}},e.event.special.tapmove={setup:function(){var t=this,o=e(t);o.on(a.moveevent,function e(n){o.data("callee",e);var i=n.originalEvent,r={position:{x:a.touch_capable?i.touches[0].pageX:n.pageX,y:a.touch_capable?i.touches[0].pageY:n.pageY},offset:{x:a.touch_capable?Math.round(i.changedTouches[0].pageX-(o.offset()?o.offset().left:0)):Math.round(n.pageX-(o.offset()?o.offset().left:0)),y:a.touch_capable?Math.round(i.changedTouches[0].pageY-(o.offset()?o.offset().top:0)):Math.round(n.pageY-(o.offset()?o.offset().top:0))},time:Date.now(),target:n.target};return w(t,"tapmove",n,r),!0})},remove:function(){e(this).off(a.moveevent,e(this).data.callee)}},e.event.special.tapend={setup:function(){var t=this,o=e(t);o.on(a.endevent,function e(n){o.data("callee",e);var i=n.originalEvent,r={position:{x:a.touch_capable?i.changedTouches[0].pageX:n.pageX,y:a.touch_capable?i.changedTouches[0].pageY:n.pageY},offset:{x:a.touch_capable?Math.round(i.changedTouches[0].pageX-(o.offset()?o.offset().left:0)):Math.round(n.pageX-(o.offset()?o.offset().left:0)),y:a.touch_capable?Math.round(i.changedTouches[0].pageY-(o.offset()?o.offset().top:0)):Math.round(n.pageY-(o.offset()?o.offset().top:0))},time:Date.now(),target:n.target};return w(t,"tapend",n,r),!0})},remove:function(){e(this).off(a.endevent,e(this).data.callee)}},e.event.special.taphold={setup:function(){var t,o=this,n=e(o),i={x:0,y:0},r=0,s=0;n.on(a.startevent,function e(p){if(p.which&&1!==p.which)return!1;n.data("tapheld",!1),t=p.target;var h=p.originalEvent,c=Date.now();a.touch_capable?h.touches[0].pageX:p.pageX,a.touch_capable?h.touches[0].pageY:p.pageY,a.touch_capable?(h.touches[0].pageX,h.touches[0].target.offsetLeft):p.offsetX,a.touch_capable?(h.touches[0].pageY,h.touches[0].target.offsetTop):p.offsetY;i.x=p.originalEvent.targetTouches?p.originalEvent.targetTouches[0].pageX:p.pageX,i.y=p.originalEvent.targetTouches?p.originalEvent.targetTouches[0].pageY:p.pageY,r=i.x,s=i.y;var u=n.parent().data("threshold")?n.parent().data("threshold"):n.data("threshold"),f=void 0!==u&&!1!==u&&parseInt(u)?parseInt(u):a.taphold_threshold;return a.hold_timer=window.setTimeout(function(){var u=i.x-r,f=i.y-s;if(p.target==t&&(i.x==r&&i.y==s||u>=-a.tap_pixel_range&&u<=a.tap_pixel_range&&f>=-a.tap_pixel_range&&f<=a.tap_pixel_range)){n.data("tapheld",!0);for(var l=Date.now()-c,g=p.originalEvent.targetTouches?p.originalEvent.targetTouches:[p],d=[],v=0;v<g.length;v++){var _={position:{x:a.touch_capable?h.changedTouches[v].pageX:p.pageX,y:a.touch_capable?h.changedTouches[v].pageY:p.pageY},offset:{x:a.touch_capable?Math.round(h.changedTouches[v].pageX-(n.offset()?n.offset().left:0)):Math.round(p.pageX-(n.offset()?n.offset().left:0)),y:a.touch_capable?Math.round(h.changedTouches[v].pageY-(n.offset()?n.offset().top:0)):Math.round(p.pageY-(n.offset()?n.offset().top:0))},time:Date.now(),target:p.target,duration:l};d.push(_)}var T=2==g.length?"taphold2":"taphold";n.data("callee1",e),w(o,T,p,d)}},f),!0}).on(a.endevent,function e(){n.data("callee2",e),n.data("tapheld",!1),window.clearTimeout(a.hold_timer)}).on(a.moveevent,function e(t){n.data("callee3",e),r=t.originalEvent.targetTouches?t.originalEvent.targetTouches[0].pageX:t.pageX,s=t.originalEvent.targetTouches?t.originalEvent.targetTouches[0].pageY:t.pageY})},remove:function(){e(this).off(a.startevent,e(this).data.callee1).off(a.endevent,e(this).data.callee2).off(a.moveevent,e(this).data.callee3)}},e.event.special.doubletap={setup:function(){var t,o,n=this,i=e(n),r=null,s=!1;i.on(a.startevent,function t(n){return(!n.which||1===n.which)&&(i.data("doubletapped",!1),n.target,i.data("callee1",t),o=n.originalEvent,r||(r={position:{x:a.touch_capable?o.touches[0].pageX:n.pageX,y:a.touch_capable?o.touches[0].pageY:n.pageY},offset:{x:a.touch_capable?Math.round(o.changedTouches[0].pageX-(i.offset()?i.offset().left:0)):Math.round(n.pageX-(i.offset()?i.offset().left:0)),y:a.touch_capable?Math.round(o.changedTouches[0].pageY-(i.offset()?i.offset().top:0)):Math.round(n.pageY-(i.offset()?i.offset().top:0))},time:Date.now(),target:n.target,element:n.originalEvent.srcElement,index:e(n.target).index()}),!0)}).on(a.endevent,function p(h){var c=Date.now(),u=c-(i.data("lastTouch")||c+1);if(window.clearTimeout(t),i.data("callee2",p),u<a.doubletap_int&&e(h.target).index()==r.index&&u>100){i.data("doubletapped",!0),window.clearTimeout(a.tap_timer);var f={position:{x:a.touch_capable?h.originalEvent.changedTouches[0].pageX:h.pageX,y:a.touch_capable?h.originalEvent.changedTouches[0].pageY:h.pageY},offset:{x:a.touch_capable?Math.round(o.changedTouches[0].pageX-(i.offset()?i.offset().left:0)):Math.round(h.pageX-(i.offset()?i.offset().left:0)),y:a.touch_capable?Math.round(o.changedTouches[0].pageY-(i.offset()?i.offset().top:0)):Math.round(h.pageY-(i.offset()?i.offset().top:0))},time:Date.now(),target:h.target,element:h.originalEvent.srcElement,index:e(h.target).index()},l={firstTap:r,secondTap:f,interval:f.time-r.time};s||(w(n,"doubletap",h,l),r=null),s=!0,window.setTimeout(function(){s=!1},a.doubletap_int)}else i.data("lastTouch",c),t=window.setTimeout(function(){r=null,window.clearTimeout(t)},a.doubletap_int,[h]);i.data("lastTouch",c)})},remove:function(){e(this).off(a.startevent,e(this).data.callee1).off(a.endevent,e(this).data.callee2)}},e.event.special.singletap={setup:function(){var t=this,o=e(t),n=null,i=null,r={x:0,y:0};o.on(a.startevent,function e(t){return(!t.which||1===t.which)&&(i=Date.now(),n=t.target,o.data("callee1",e),r.x=t.originalEvent.targetTouches?t.originalEvent.targetTouches[0].pageX:t.pageX,r.y=t.originalEvent.targetTouches?t.originalEvent.targetTouches[0].pageY:t.pageY,!0)}).on(a.endevent,function e(s){if(o.data("callee2",e),s.target==n){var p=s.originalEvent.changedTouches?s.originalEvent.changedTouches[0].pageX:s.pageX,h=s.originalEvent.changedTouches?s.originalEvent.changedTouches[0].pageY:s.pageY;a.tap_timer=window.setTimeout(function(){var e=r.x-p,n=r.y-h;if(!o.data("doubletapped")&&!o.data("tapheld")&&(r.x==p&&r.y==h||e>=-a.tap_pixel_range&&e<=a.tap_pixel_range&&n>=-a.tap_pixel_range&&n<=a.tap_pixel_range)){var c=s.originalEvent,u={position:{x:a.touch_capable?c.changedTouches[0].pageX:s.pageX,y:a.touch_capable?c.changedTouches[0].pageY:s.pageY},offset:{x:a.touch_capable?Math.round(c.changedTouches[0].pageX-(o.offset()?o.offset().left:0)):Math.round(s.pageX-(o.offset()?o.offset().left:0)),y:a.touch_capable?Math.round(c.changedTouches[0].pageY-(o.offset()?o.offset().top:0)):Math.round(s.pageY-(o.offset()?o.offset().top:0))},time:Date.now(),target:s.target};u.time-i<a.taphold_threshold&&w(t,"singletap",s,u)}},a.doubletap_int)}})},remove:function(){e(this).off(a.startevent,e(this).data.callee1).off(a.endevent,e(this).data.callee2)}},e.event.special.tap={setup:function(){var t,o,n=this,i=e(n),r=!1,s=null,p={x:0,y:0};i.on(a.startevent,function e(a){return i.data("callee1",e),(!a.which||1===a.which)&&(r=!0,p.x=a.originalEvent.targetTouches?a.originalEvent.targetTouches[0].pageX:a.pageX,p.y=a.originalEvent.targetTouches?a.originalEvent.targetTouches[0].pageY:a.pageY,t=Date.now(),s=a.target,o=a.originalEvent.targetTouches?a.originalEvent.targetTouches:[a],!0)}).on(a.endevent,function e(h){i.data("callee2",e);var c=h.originalEvent.targetTouches?h.originalEvent.changedTouches[0].pageX:h.pageX,u=h.originalEvent.targetTouches?h.originalEvent.changedTouches[0].pageY:h.pageY,f=p.x-c,l=p.y-u;if(s==h.target&&r&&Date.now()-t<a.taphold_threshold&&(p.x==c&&p.y==u||f>=-a.tap_pixel_range&&f<=a.tap_pixel_range&&l>=-a.tap_pixel_range&&l<=a.tap_pixel_range)){for(var g=h.originalEvent,d=[],v=0;v<o.length;v++){var _={position:{x:a.touch_capable?g.changedTouches[v].pageX:h.pageX,y:a.touch_capable?g.changedTouches[v].pageY:h.pageY},offset:{x:a.touch_capable?Math.round(g.changedTouches[v].pageX-(i.offset()?i.offset().left:0)):Math.round(h.pageX-(i.offset()?i.offset().left:0)),y:a.touch_capable?Math.round(g.changedTouches[v].pageY-(i.offset()?i.offset().top:0)):Math.round(h.pageY-(i.offset()?i.offset().top:0))},time:Date.now(),target:h.target};d.push(_)}var T=2==o.length?"tap2":"tap";w(n,T,h,d)}})},remove:function(){e(this).off(a.startevent,e(this).data.callee1).off(a.endevent,e(this).data.callee2)}},e.event.special.swipe={setup:function(){var t,o=e(this),n=!1,i=!1,r={x:0,y:0},s={x:0,y:0};o.on(a.startevent,function i(p){(o=e(p.currentTarget)).data("callee1",i),r.x=p.originalEvent.targetTouches?p.originalEvent.targetTouches[0].pageX:p.pageX,r.y=p.originalEvent.targetTouches?p.originalEvent.targetTouches[0].pageY:p.pageY,s.x=r.x,s.y=r.y,n=!0;var h=p.originalEvent;t={position:{x:a.touch_capable?h.touches[0].pageX:p.pageX,y:a.touch_capable?h.touches[0].pageY:p.pageY},offset:{x:a.touch_capable?Math.round(h.changedTouches[0].pageX-(o.offset()?o.offset().left:0)):Math.round(p.pageX-(o.offset()?o.offset().left:0)),y:a.touch_capable?Math.round(h.changedTouches[0].pageY-(o.offset()?o.offset().top:0)):Math.round(p.pageY-(o.offset()?o.offset().top:0))},time:Date.now(),target:p.target}}),o.on(a.moveevent,function p(h){var c;(o=e(h.currentTarget)).data("callee2",p),s.x=h.originalEvent.targetTouches?h.originalEvent.targetTouches[0].pageX:h.pageX,s.y=h.originalEvent.targetTouches?h.originalEvent.targetTouches[0].pageY:h.pageY;var u=o.parent().data("xthreshold")?o.parent().data("xthreshold"):o.data("xthreshold"),f=o.parent().data("ythreshold")?o.parent().data("ythreshold"):o.data("ythreshold"),l=void 0!==u&&!1!==u&&parseInt(u)?parseInt(u):a.swipe_h_threshold,g=void 0!==f&&!1!==f&&parseInt(f)?parseInt(f):a.swipe_v_threshold;if(r.y>s.y&&r.y-s.y>g&&(c="swipeup"),r.x<s.x&&s.x-r.x>l&&(c="swiperight"),r.y<s.y&&s.y-r.y>g&&(c="swipedown"),r.x>s.x&&r.x-s.x>l&&(c="swipeleft"),void 0!=c&&n){r.x=0,r.y=0,s.x=0,s.y=0,n=!1;var d=h.originalEvent,v={position:{x:a.touch_capable?d.touches[0].pageX:h.pageX,y:a.touch_capable?d.touches[0].pageY:h.pageY},offset:{x:a.touch_capable?Math.round(d.changedTouches[0].pageX-(o.offset()?o.offset().left:0)):Math.round(h.pageX-(o.offset()?o.offset().left:0)),y:a.touch_capable?Math.round(d.changedTouches[0].pageY-(o.offset()?o.offset().top:0)):Math.round(h.pageY-(o.offset()?o.offset().top:0))},time:Date.now(),target:h.target},w=Math.abs(t.position.x-v.position.x),_=Math.abs(t.position.y-v.position.y),T={startEvnt:t,endEvnt:v,direction:c.replace("swipe",""),xAmount:w,yAmount:_,duration:v.time-t.time};i=!0,o.trigger("swipe",T).trigger(c,T)}}),o.on(a.endevent,function r(s){var p="";if((o=e(s.currentTarget)).data("callee3",r),i){var h=o.data("xthreshold"),c=o.data("ythreshold"),u=void 0!==h&&!1!==h&&parseInt(h)?parseInt(h):a.swipe_h_threshold,f=void 0!==c&&!1!==c&&parseInt(c)?parseInt(c):a.swipe_v_threshold,l=s.originalEvent,g={position:{x:a.touch_capable?l.changedTouches[0].pageX:s.pageX,y:a.touch_capable?l.changedTouches[0].pageY:s.pageY},offset:{x:a.touch_capable?Math.round(l.changedTouches[0].pageX-(o.offset()?o.offset().left:0)):Math.round(s.pageX-(o.offset()?o.offset().left:0)),y:a.touch_capable?Math.round(l.changedTouches[0].pageY-(o.offset()?o.offset().top:0)):Math.round(s.pageY-(o.offset()?o.offset().top:0))},time:Date.now(),target:s.target};t.position.y>g.position.y&&t.position.y-g.position.y>f&&(p="swipeup"),t.position.x<g.position.x&&g.position.x-t.position.x>u&&(p="swiperight"),t.position.y<g.position.y&&g.position.y-t.position.y>f&&(p="swipedown"),t.position.x>g.position.x&&t.position.x-g.position.x>u&&(p="swipeleft");var d=Math.abs(t.position.x-g.position.x),v=Math.abs(t.position.y-g.position.y),w={startEvnt:t,endEvnt:g,direction:p.replace("swipe",""),xAmount:d,yAmount:v,duration:g.time-t.time};o.trigger("swipeend",w)}n=!1,i=!1})},remove:function(){e(this).off(a.startevent,e(this).data.callee1).off(a.moveevent,e(this).data.callee2).off(a.endevent,e(this).data.callee3)}},e.event.special.scrollstart={setup:function(){var t,o,n=this,i=e(n);function r(e,a){w(n,(t=a)?"scrollstart":"scrollend",e)}i.on(a.scrollevent,function e(a){i.data("callee",e),t||r(a,!0),clearTimeout(o),o=setTimeout(function(){r(a,!1)},50)})},remove:function(){e(this).off(a.scrollevent,e(this).data.callee)}};var o,n,i,r,s=e(window),p={0:!0,180:!0};if(a.orientation_support){var h=window.innerWidth||s.width(),c=window.innerHeight||s.height();i=h>c&&h-c>50,r=p[window.orientation],(i&&r||!i&&!r)&&(p={"-90":!0,90:!0})}function u(){var e=o();e!==n&&(n=e,s.trigger("orientationchange"))}e.event.special.orientationchange={setup:function(){return!a.orientation_support&&(n=o(),s.on("throttledresize",u),!0)},teardown:function(){return!a.orientation_support&&(s.off("throttledresize",u),!0)},add:function(e){var t=e.handler;e.handler=function(e){return e.orientation=o(),t.apply(this,arguments)}}},e.event.special.orientationchange.orientation=o=function(){var e=document.documentElement;return(a.orientation_support?p[window.orientation]:e&&e.clientWidth/e.clientHeight<1.1)?"portrait":"landscape"},e.event.special.throttledresize={setup:function(){e(this).on("resize",d)},teardown:function(){e(this).off("resize",d)}};var f,l,g,d=function(){l=Date.now(),(g=l-v)>=250?(v=l,e(this).trigger("throttledresize")):(f&&window.clearTimeout(f),f=window.setTimeout(u,250-g))},v=0;function w(t,a,o,n){var i=o.type;o.type=a,e.event.dispatch.call(t,o,n),o.type=i}e.each({scrollend:"scrollstart",swipeup:"swipe",swiperight:"swipe",swipedown:"swipe",swipeleft:"swipe",swipeend:"swipe",tap2:"tap",taphold2:"taphold"},function(t,a){e.event.special[t]={setup:function(){e(this).on(a,e.noop)}}})}(jQuery);

function getParamsFromUrl() {
	var query = location.search.substr(1).replace(/%5B/g, '[').replace(/%5D/g, ']');
	var params = query.split("&");
	var result = [];
	for(var i=0; i<params.length; i++) {
		var item = params[i].split("=");
	
		const key = item[0].replace(/\[|\]/g, '')
		const value = item[1]
	
		if(!result[key]) result[key] = [value]
		else result[key].push(value)
	
	}
	return result;
}

(function($) {

	"use strict";

	//var $ = 'jQuery';

	$(document).ready(function() {
		  
		$('.select-miasto').select2({
			placeholder: "Dowolne",
		});

		$('.select-dzielnica').select2({
			placeholder: "Dowolne",
		});

		var urlMoje = getParamsFromUrl();
		var miasta = urlMoje.location;
		var dzielnice = urlMoje.dzielnica;
		//var miasta_string = miasta.join(", ");

		jQuery('#select-location').val(miasta).trigger('change');
		jQuery('#select-dzielnica').val(dzielnice).trigger('change');

		// $.each(miasta_string.split(","), function(i,e){
		// 	$("#select-location option[value='" + e + "']").prop("selected", true);
		// });
		// $('#select-location').select2().trigger('change');

		// urlMoje.location.each(function(){
		// 	$('#select-location').val(index);
		// 	$('#select-location').select2().trigger('change');
		// });

		if (/iPhone/.test(navigator.userAgent) && !window.MSStream)
	    {
	        $(document).on("focus", "input, textarea, select", function()
	        {
	            $('meta[name=viewport]').remove();
	            $('head').append('<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">');
	        });

	        $(document).on("blur", "input, textarea, select", function()
	        {
	            $('meta[name=viewport]').remove();
	            $('head').append('<meta name="viewport" content="width=device-width, initial-scale=1">');
	        });
	    }

		/*
		 * clone gallery
		 */
		
		/*
		 * gallery
		 */
		
		const player = new Plyr('#playerVimeo', {});
		//const player2 = new Plyr('#player', {});
		
		$('.search-show-more').click(function(){
			$('.search-carousel').toggleClass('open');
		})
		// NAVIGATION MENU
  
  // menu icon states, opening main nav
  $('#menu-icon').click(function(){
  	console.log("sagsgasga");
    $(this).toggleClass('open');
    $('#menu,#menu-toggle,#page-content,#menu-overlay').toggleClass('open');
    $('#menu li,.submenu-toggle').removeClass('open');
  });
  
  // clicking on overlay closes menu
  $('#menu-overlay').click(function(){
    $('*').removeClass('open');
  });
  
  // add child menu toggles and parent class
  $('#menu li').has('ul').addClass('parent').prepend('<div class="submenu-toggle">open</div>');
  
  // toggle child menus
  $('.submenu-toggle').click(function(){
    var currentToggle=$(this);
    currentToggle.parent().toggleClass('open');
    currentToggle.toggleClass('open');
  });
		
		var klon = $('#miniaturki');
		klon.prependTo('.content');

		// var klon = $("#property-carousel-two").clone();
		// klon.wrap( "<div class='new'></div>" );
		// klon.attr("id", "property-carousel-two dwa");

		//console.log(klon);
		//klon.appendTo($("#property-slider-two-wrapper"));
		//$("#property-carousel-two").remove();appendTo($("#content"));
		/*
		 *
		 */
		/*
		 * tabele z szczegolami
		 */
		$("tr").each(function() {
			var trIsEmpty = true;
			var tr = $(this);

			tr.find("td:not(.requirementRight)").each(function() {
				var td = $(this);

				if (isEmpty(td) === false) {
					trIsEmpty = false;
				}
			});

			if (trIsEmpty == true) {
				tr.hide();
			}
		});

		function isEmpty(td) {
			if (td.text == '' || td.text() == ' ' || td.html() == '') {
				return true;
			}

			return false;
		}

		/*
		 * lang switcher
		 */

		if ($(".lang-switch")[0]) {
			//console.log("sa jezyki");
		} else {
			$('.second-meta-box').css('display', 'none');
		}

		$("#tab1C p:empty").remove();
		$(".lang-switch p:empty").remove();
		$('.lang-switch').each(function() {
			if ($(this).next().is('br')) {
				$(this).next().remove();
			}
		});

		$('.second-meta-box li a:not(:first)').addClass('inactive');
		$('.lang-switch:not(:first)').addClass('en');
		// $('.lang-switch').hide();
		// $('.lang-switch:first').show();

		$('.second-meta-box li a').click(function() {
			if ($(this).hasClass('inactive')) {//this is the start of our condition
				$('.en-lang').toggleClass('inactive');
				$('.pl-lang').toggleClass('active');
				$('.second-meta-box li a').addClass('inactive');
				$(this).removeClass('inactive');
				$('.en-lang p').first().addClass('shortParagraph');
				$('.pl-lang p').first().addClass('shortParagraph');
			}
		});

		/*
		 * tabs
		 */

		$('#tabs li a:not(:first)').addClass('inactive');
		$('.my-tabs').hide();
		$('.my-tabs:first').show();

		$('#tabs li a').click(function() {
			var t = $(this).attr('id');
			if ($(this).hasClass('inactive')) {//this is the start of our condition
				$('#tabs li a').addClass('inactive');
				$(this).removeClass('inactive');

				$('.my-tabs').hide();
				$('#' + t + 'C').fadeIn('slow');
				//window.onload = initialize_property_map();
			}
		});
		
		//window.onload = initialize_property_map();

		/*
		 *
		 */

		if ($('#menu-main-menu li').hasClass("current-menu-ancestor")) {
			//console.log("noSearch");
			//$('body').addClass("noSearch");
		}
		//$('body.noSearch advance-search ').css("display", "none");

		$.fn.digits = function() {
			return this.each(function() {
				$(this).text($(this).text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1 "));
			})
		}
		$("span.cena-metr").digits();
		/*
		 * clone pagination
		 */
		var c = 0;
		var pagination_top = $('#pagination-bottom');
		var prop_page = $('.top-pagination');
		pagination_top.clone().appendTo(prop_page);
		//console.log(pagination_top);

		/*-----------------------------------------------------------------------------------*/
		/* For RTL Languages
		 /*-----------------------------------------------------------------------------------*/
		if ($('body').hasClass('rtl')) {
			$('.contact-number .fa-phone,' + '.more-details .fa-caret-right').addClass('fa-flip-horizontal');
		}

		/*-----------------------------------------------------------------------------------*/
		/* Cross Browser
		 /*-----------------------------------------------------------------------------------*/
		$('.property-item .features span:last-child').css('border', 'none');
		$('.dsidx-prop-title').css('margin', '0 0 15px 0');
		$('.dsidx-prop-summary a img').css('border', 'none');

		/*-----------------------------------------------------------------------------------*/
		/* Main Menu Dropdown Control
		 /*-----------------------------------------------------------------------------------*/
		$('.main-menu ul li').hover(function() {
			$(this).children('ul').stop(true, true).fadeIn(100);
		}, function() {
			$(this).children('ul').stop(true, true).fadeOut(100);
		});

		/*-----------------------------------------------------------------------------------*/
		/*	Responsive Nav
		 /*-----------------------------------------------------------------------------------*/
		var $mainNav = $('.main-menu > div > ul');
		var optionsList = '<option value="" selected>' + localized.nav_title + '</option>';

		$mainNav.find('li').each(function() {
			var $this = $(this),
			    $anchor = $this.children('a'),
			    depth = $this.parents('ul').length - 1,
			    indent = '';
			if (depth) {
				while (depth > 0) {
					indent += ' - ';
					depth--;
				}
			}
			optionsList += '<option value="' + $anchor.attr('href') + '">' + indent + ' ' + $anchor.text() + '</option>';
		}).end().last().after('<select class="responsive-nav">' + optionsList + '</select>');

		$('.responsive-nav').on('change', function() {
			window.location = $(this).val();
		});

		/*-----------------------------------------------------------------------------------*/
		/*	Flex Slider
		 /*-----------------------------------------------------------------------------------*/
		if (jQuery().flexslider) {
			// Flex Slider for Homepage
			$('#home-flexslider .flexslider').flexslider({
				touch : true,
				animation : "fade",
				slideshowSpeed : 7000,
				animationSpeed : 1500,
				directionNav : true,
				controlNav : false,
				keyboardNav : true,
				start : function(slider) {
					slider.removeClass('loading');
					var slide_count = slider.count - 1;

					$(slider).find('img.lazy:eq(0)').each(function() {
						var src = $(this).attr('data-src');
						$(this).attr('src', src).removeAttr('data-src');
					});
				},
				before : function(slider) {// Fires asynchronously with each slider animation
					var slides = slider.slides,
					    index = slider.animatingTo,
					    $slide = $(slides[index]),
					    $img = $slide.find('img[data-src]'),
					    current =
					    index,
					    nxt_slide = current + 1,
					    prev_slide = current - 1;

					$slide.parent().find('img.lazy:eq(' + current + '), img.lazy:eq(' + prev_slide + '), img.lazy:eq(' + nxt_slide + ')').each(function() {
						var src = $(this).attr('data-src');
						$(this).attr('src', src).removeAttr('data-src');
					});
				}
			});

			// Remove Flex Slider Navigation for Smaller Screens Like IPhone Portrait
			$('.slider-wrapper , .listing-slider').hover(function() {
				var mobile = $('body').hasClass('probably-mobile');
				if (!mobile) {
					$('.flex-direction-nav').stop(true, true).fadeIn('slow');
				}
			}, function() {
				$('.flex-direction-nav').stop(true, true).fadeOut('slow');
			});

			// Flex Slider for Detail Page
			$('#property-detail-flexslider .flexslider').flexslider({
				animation : "slide",
				directionNav : false,
				controlNav : "thumbnails"
			});

			// Flex Slider Gallery Post
			$('.listing-slider ').flexslider({
				animation : "slide"
			});

			/* Property detail page slider variation two */
			$('#property-carousel-two').flexslider({
				animation : "slide",
				controlNav : false,
				animationLoop : false,
				slideshow : false,
				itemWidth : 113,
				itemMargin : 10,
				// move: 1,
				asNavFor : '#property-slider-two'
			});
			$('#property-slider-two').flexslider({
				animation : "slide",
				directionNav : true,
				controlNav : false,
				animationLoop : false,
				slideshow : true,
				sync : "#property-carousel-two",
				start : function(slider) {// Fires when the slider loads the first slide
					var slide_count = slider.count - 1;

					$(slider).find('img.lazy:eq(0)').each(function() {
						var src = $(this).attr('data-src');
						$(this).attr('src', src).removeAttr('data-src');
					});
				},
				before : function(slider) {// Fires asynchronously with each slider animation
					var slides = slider.slides,
					    index = slider.animatingTo,
					    $slide = $(slides[index]),
					    $img = $slide.find('img[data-src]'),
					    current =
					    index,
					    nxt_slide = current + 1,
					    prev_slide = current - 1;

					$slide.parent().find('img.lazy:eq(' + current + '), img.lazy:eq(' + prev_slide + '), img.lazy:eq(' + nxt_slide + ')').each(function() {
						var src = $(this).attr('data-src');
						$(this).attr('src', src).removeAttr('data-src');
					});
				}
			});

		}

		/*-----------------------------------------------------------------------------------*/
		/*	jCarousel
		 /*-----------------------------------------------------------------------------------*/
		if (jQuery().jcarousel) {
			// Jcarousel for Detail Page
			jQuery('#property-detail-flexslider .flex-control-nav').jcarousel({
				vertical : true,
				scroll : 1
			});

			// Jcarousel for partners
			jQuery('.brands-carousel .brands-carousel-list ').jcarousel({
				scroll : 1
			});
		}

		/*-----------------------------------------------------------------------------------*/
		/*	Carousel - Elastislide
		 /*-----------------------------------------------------------------------------------*/
		var param = {
			speed : 500,
			imageW : 245,
			minItems : 1,
			margin : 30,
			onClick : function($object) {
				window.location = $object.find('a').first().attr('href');
				return true;
			}
		};

		function cstatus(a, b, c) {
			var temp = a.children("li");
			temp.last().attr('style', 'margin-right: 0px !important');
			if (temp.length > c) {
				b.elastislide(param);
			}
		};

		if (jQuery().elastislide) {
			var fp = $('.featured-properties-carousel .es-carousel-wrapper ul'),
			    fpCarousel = $('.featured-properties-carousel .carousel');

			cstatus(fp, fpCarousel, 4);
		}

		/*-------------------------------------------------------*/
		/*	Select Box
		 /* -----------------------------------------------------*/
		if (jQuery().selectbox) {
			$('.search-select').selectbox();

			/* dropdown fix - to close dropdown if opened by clicking anywhere out */
			$('body').on('click', function(e) {
				if ($(e.target).hasClass('selectbox'))
					return;
				$('.selectbox-wrapper').css('display', 'none');
			});
		}

		/*-------------------------------------------------------*/
		/*	 Focus and Blur events with input elements
		 /* -----------------------------------------------------*/
		var addFocusAndBlur = function($input, $val) {
			$input.focus(function() {
				if ($(this).value == $val) {
					$(this).value = '';
				}
			});

			$input.blur(function() {
				if ($(this).value == '') {
					$(this).value = $val;
				}
			});
		};

		// Attach the events
		addFocusAndBlur(jQuery('#principal'), 'Principal');
		addFocusAndBlur(jQuery('#interest'), 'Interest');
		addFocusAndBlur(jQuery('#payment'), 'Payment');
		addFocusAndBlur(jQuery('#texes'), 'Texes');
		addFocusAndBlur(jQuery('#insurance'), 'Insurance');
		addFocusAndBlur(jQuery('#pmi'), 'PMI');
		addFocusAndBlur(jQuery('#extra'), 'Extra');

		/*    addFocusAndBlur(jQuery('.agent-detail .contact-form #name'),'Name');
		 addFocusAndBlur(jQuery('.agent-detail .contact-form #email'),'Email');
		 addFocusAndBlur(jQuery('.agent-detail .contact-form #comment'),'Message'); */

		/*-----------------------------------------------------------------------------------*/
		/*	Apply Bootstrap Classes on Comment Form Fields to Make it Responsive
		 /*-----------------------------------------------------------------------------------*/
		$('#respond #submit, #dsidx-contact-form-submit').addClass('real-btn');
		$('.pages-nav > a').addClass('real-btn');
		$('.dsidx-search-button .submit').addClass('real-btn');
		$('.wpcf7-submit').addClass('real-btn');

		/*----------------------------------------------------------------------------------*/
		/* Contact Form AJAX validation and submission
		 /* Validation Plugin : http://bassistance.de/jquery-plugins/jquery-plugin-validation/
		 /* Form Ajax Plugin : http://www.malsup.com/jquery/form/
		 /*---------------------------------------------------------------------------------- */
		if (jQuery().validate && jQuery().ajaxSubmit) {
			// Contact Form Handling
			var contact_options = {
				target : '#message-sent',
				beforeSubmit : function() {
					$('#contact-loader').fadeIn('fast');
					$('#message-sent').fadeOut('fast');
				},
				success : function() {
					$('#contact-loader').fadeOut('fast');
					$('#message-sent').fadeIn('fast');
				}
			};

			$('#contact-form .contact-form').validate({
				errorLabelContainer : $("div.error-container"),
				submitHandler : function(form) {
					$(form).ajaxSubmit(contact_options);
				}
			});

			// Agent Message Form Handling
			var agent_form_options = {
				target : '#message-sent',
				beforeSubmit : function() {
					$('#contact-loader').fadeIn('fast');
					$('#message-sent').fadeOut('fast');
				},
				success : function() {
					$('#contact-loader').fadeOut('fast');
					$('#message-sent').fadeIn('fast');
				}
			};

			$('#agent-contact-form').validate({
				errorLabelContainer : $("div.error-container"),
				submitHandler : function(form) {
					$(form).ajaxSubmit(agent_form_options);
				}
			});

		}

		/*-----------------------------------------------------------------------------------*/
		/* Swipe Box Lightbox
		 /*-----------------------------------------------------------------------------------*/
		if (jQuery().swipebox) {
			$(".swipebox").swipebox();
		}

		/*-----------------------------------------------------------------------------------*/
		/* Pretty Photo Lightbox
		 /*-----------------------------------------------------------------------------------*/
		if (jQuery().prettyPhoto) {
			$(".pretty-photo").prettyPhoto({
				deeplinking : false,
				social_tools : false
			});

			$('a[data-rel]').each(function() {
				$(this).attr('rel', $(this).data('rel'));
			});

			$("a[rel^='prettyPhoto']").prettyPhoto({
				overlay_gallery : false,
				social_tools : false
			});
		}

		/*-------------------------------------------------------*/
		/*	Isotope
		 /*------------------------------------------------------*/
		if (jQuery().isotope) {
			$(function() {

				var container = $('.isotope'),
				    filterLinks = $('#filter-by a');

				filterLinks.click(function(e) {
					var selector = $(this).attr('data-filter');
					container.isotope({
						filter : '.' + selector,
						itemSelector : '.isotope-item',
						layoutMode : 'fitRows',
						animationEngine : 'best-available'
					});

					filterLinks.removeClass('active');
					$('#filter-by li').removeClass('current-cat');
					$(this).addClass('active');
					e.preventDefault();
				});

				/* to fix floating bugs due to variation in height */
				setTimeout(function() {
					container.isotope({
						filter : "*",
						layoutMode : 'fitRows',
						itemSelector : '.isotope-item',
						animationEngine : 'best-available'
					});
				}, 1000);

			});
		}

		/* ---------------------------------------------------- */
		/*	Gallery Hover Effect
		 /* ---------------------------------------------------- */

		var $mediaContainer = $('.gallery-item .media_container'),
		    $media = $('.gallery-item .media_container a');

		var $margin = -($media.height() / 2);
		$media.css('margin-top', $margin);

		$(function() {

			$('.gallery-item figure').hover(function() {
				var media = $media.width(),
				    container = ($mediaContainer.width() / 2) - (media + 2);

				$(this).children('.media_container').stop().fadeIn(300);
				$(this).find('.media_container').children('a.link').stop().animate({
					'right' : container
				}, 300);
				$(this).find('.media_container').children('a.zoom').stop().animate({
					'left' : container
				}, 300);
			}, function() {
				$(this).children('.media_container').stop().fadeOut(300);
				$(this).find('.media_container').children('a.link').stop().animate({
					'right' : '0'
				}, 300);
				$(this).find('.media_container').children('a.zoom').stop().animate({
					'left' : '0'
				}, 300);
			});

		});

		/* ---------------------------------------------------- */
		/*  Sizing Header Outer Strip
		 /* ---------------------------------------------------- */
		function outer_strip() {
			var $item = $('.outer-strip'),
			    $c_width = $('.header-wrapper .container').width(),
			    $w_width = $(window).width(),
			    $i_width = ($w_width - $c_width) / 2;

			if ($('body').hasClass('rtl')) {
				$item.css({
					left : -$i_width,
					width : $i_width
				});
			} else {
				$item.css({
					right : -$i_width,
					width : $i_width
				});
			}
		}

		outer_strip();
		$(window).resize(function() {
			outer_strip();
		});

		/* ---------------------------------------------------- */
		/*	Notification Hide Function
		 /* ---------------------------------------------------- */
		$(".icon-remove").click(function() {
			$(this).parent().fadeOut(300);
		});

		/*-----------------------------------------------------------------------------------*/
		/*	Image Hover Effect
		 /*-----------------------------------------------------------------------------------*/
		if (jQuery().transition) {
			$('.zoom_img_box img').hover(function() {
				$(this).stop(true, true).transition({
					scale : 1.1
				}, 300);
			}, function() {
				$(this).stop(true, true).transition({
					scale : 1
				}, 150);
			});
		}

		/*-----------------------------------------------------------------------------------*/
		/*	Grid and Listing Toggle View
		 /*-----------------------------------------------------------------------------------*/
		if ($('.lisitng-grid-layout').hasClass('property-toggle')) {
			$('.listing-layout  .property-item-grid').hide();
			$('a.grid').on('click', function() {
				$('.listing-layout').addClass('property-grid');
				$('.property-item-grid').show();
				$('.property-item-list').hide();
				$('a.grid').addClass('active');
				$('a.list').removeClass('active');
			});
			$('a.list').on('click', function() {
				$('.listing-layout').removeClass('property-grid');
				$('.property-item-grid').hide();
				$('.property-item-list').show();
				$('a.grid').removeClass('active');
				$('a.list').addClass('active');
			});
		}

		/*-----------------------------------------------------------------------------------*/
		/* Calendar Widget Border Fix
		 /*-----------------------------------------------------------------------------------*/
		var $calendar = $('.sidebar .widget #wp-calendar');
		if ($calendar.length > 0) {
			$calendar.each(function() {
				$(this).closest('.widget').css('border', 'none').css('background', 'transparent');
			});
		}

		var $single_listing = $('.sidebar .widget .dsidx-widget-single-listing');
		if ($single_listing.length > 0) {
			$single_listing.each(function() {
				$(this).closest('.widget').css('border', 'none').css('background', 'transparent');
			});
		}

		/*-----------------------------------------------------------------------------------*/
		/*	Tags Cloud
		 /*-----------------------------------------------------------------------------------*/
		$('.tagcloud').addClass('clearfix');
		$('.tagcloud a').removeAttr('style');

		/*-----------------------------------------------------------------------------------*/
		/*	Max and Min Price Related JavaScript - to show red outline of min is bigger than max
		 /*-----------------------------------------------------------------------------------*/
		$('#select-min-price,#select-max-price').change(function(obj, e) {
			var min_text_val = $('#select-min-price').val();
			var min_int_val = (isNaN(min_text_val)) ? 0 : parseInt(min_text_val);

			var max_text_val = $('#select-max-price').val();
			var max_int_val = (isNaN(max_text_val)) ? 0 : parseInt(max_text_val);

			if ((min_int_val >= max_int_val) && (min_int_val != 0) && (max_int_val != 0)) {
				$('#select-max-price_input,#select-min-price_input').css('outline', '2px solid red');
			} else {
				$('#select-max-price_input,#select-min-price_input').css('outline', 'none');
			}
		});

		$('#select-min-price-for-rent, #select-max-price-for-rent').change(function(obj, e) {
			var min_text_val = $('#select-min-price-for-rent').val();
			var min_int_val = (isNaN(min_text_val)) ? 0 : parseInt(min_text_val);

			var max_text_val = $('#select-max-price-for-rent').val();
			var max_int_val = (isNaN(max_text_val)) ? 0 : parseInt(max_text_val);

			if ((min_int_val >= max_int_val) && (min_int_val != 0) && (max_int_val != 0)) {
				$('#select-max-price-for-rent_input,#select-min-price-for-rent_input').css('outline', '2px solid red');
			} else {
				$('#select-max-price-for-rent_input,#select-min-price-for-rent_input').css('outline', 'none');
			}
		});

		/*-----------------------------------------------------------------------------------*/
		/*	Max and Min Area Related JavaScript - to show red outline of min is bigger than max
		 /*-----------------------------------------------------------------------------------*/
		$('#min-area,#max-area').change(function(obj, e) {
			var min_text_val = $('#min-area').val();
			var min_int_val = (isNaN(min_text_val)) ? 0 : parseInt(min_text_val);

			var max_text_val = $('#max-area').val();
			var max_int_val = (isNaN(max_text_val)) ? 0 : parseInt(max_text_val);

			if ((min_int_val >= max_int_val) && (min_int_val != 0) && (max_int_val != 0)) {
				$('#min-area,#max-area').css('outline', '2px solid red');
			} else {
				$('#min-area,#max-area').css('outline', 'none');
			}
		});

		/*-----------------------------------------------------------------------------------*/
		/*	Property ID Change Event
		/*-----------------------------------------------------------------------------------*/
		/*$('.advance-search-form #property-id-txt').change(function(obj, e){
		var search_form = $(this).closest('form.advance-search-form');
		var input_controls = search_form.find('input').not('#property-id-txt, .real-btn');
		if( $(this).val().length > 0  ){
		input_controls.prop('disabled', true);
		}else{
		input_controls.prop('disabled', false);
		}
		});*/

		/*-----------------------------------------------------------------------------------*/
		/* Submit Property Page
		/*-----------------------------------------------------------------------------------*/

		// Google Map
		var mapField = {};

		(function() {

			var thisMapField = this;

			this.container = null;
			this.canvas = null;
			this.latlng = null;
			this.map = null;
			this.marker = null;
			this.geocoder = null;

			this.init = function($container) {
				this.container = $container;
				this.canvas = $container.find('.map-canvas');
				this.initLatLng(53.346881, -6.258860);
				this.initMap();
				this.initMarker();
				this.initGeocoder();
				this.initMarkerPosition();
				this.initListeners();
				this.initAutoComplete();
				this.bindHandlers();
			}

			this.initLatLng = function($lat, $lng) {
				this.latlng = new window.google.maps.LatLng($lat, $lng);
			}

			this.initMap = function() {
				this.map = new window.google.maps.Map(this.canvas[0], {
					zoom : 8,
					center : this.latlng,
					streetViewControl : 0,
					mapTypeId : window.google.maps.MapTypeId.ROADMAP
				});
			}

			this.initMarker = function() {
				this.marker = new window.google.maps.Marker({
					position : this.latlng,
					map : this.map,
					draggable : true
				});
			}

			this.initMarkerPosition = function() {
				var coord = this.container.find('.map-coordinate').val();
				var addressField = this.container.find('.goto-address-button').val();
				var l;
				var zoom;

				if (coord) {
					l = coord.split(',');
					this.marker.setPosition(new window.google.maps.LatLng(l[0], l[1]));

					zoom = l.length > 2 ? parseInt(l[2], 10) : 15;

					this.map.setCenter(this.marker.position);
					this.map.setZoom(zoom);
				} else if (addressField) {
					this.geocodeAddress(addressField);
				}

			}

			this.initGeocoder = function() {
				this.geocoder = new window.google.maps.Geocoder();
			}

			this.initListeners = function() {
				var that = thisMapField;
				window.google.maps.event.addListener(this.map, 'click', function(event) {
					that.marker.setPosition(event.latLng);
					that.updatePositionInput(event.latLng);
				});
				window.google.maps.event.addListener(this.marker, 'drag', function(event) {
					that.updatePositionInput(event.latLng);
				});
			}

			this.updatePositionInput = function(latLng) {
				this.container.find('.map-coordinate').val(latLng.lat() + ',' + latLng.lng());
			}

			this.geocodeAddress = function(addressField) {
				var address = '';
				var fieldList = addressField.split(',');
				var loop;

				for ( loop = 0; loop < fieldList.length; loop++) {
					address += jQuery('#' + fieldList[loop]).val();
					if (loop + 1 < fieldList.length) {
						address += ', ';
					}
				}

				address = address.replace(/\n/g, ',');
				address = address.replace(/,,/g, ',');

				var that = thisMapField;
				this.geocoder.geocode({
					'address' : address
				}, function(results, status) {
					if (status == window.google.maps.GeocoderStatus.OK) {
						that.updatePositionInput(results[0].geometry.location);
						that.marker.setPosition(results[0].geometry.location);
						that.map.setCenter(that.marker.position);
						that.map.setZoom(15);
					}
				});
			}

			this.initAutoComplete = function() {
				var addressField = this.container.find('.goto-address-button').val();
				if (!addressField)
					return null;

				var that = thisMapField;
				$('#' + addressField).autocomplete({
					source : function(request, response) {
						// TODO: add 'region' option, to help bias geocoder.
						that.geocoder.geocode({
							'address' : request.term
						}, function(results, status) {
							response($.map(results, function(item) {
								return {
									label : item.formatted_address,
									value : item.formatted_address,
									latitude : item.geometry.location.lat(),
									longitude : item.geometry.location.lng()
								};
							}));
						});
					},
					select : function(event, ui) {
						that.container.find(".map-coordinate").val(ui.item.latitude + ',' + ui.item.longitude);
						var location = new window.google.maps.LatLng(ui.item.latitude, ui.item.longitude);
						that.map.setCenter(location);
						// Drop the Marker
						setTimeout(function() {
							that.marker.setValues({
								position : location,
								animation : window.google.maps.Animation.DROP
							});
						}, 1500);
					}
				});
			}

			this.bindHandlers = function() {
				var that = thisMapField;
				this.container.find('.goto-address-button').bind('click', function() {
					that.onFindAddressClick($(this));
				});
			}

			this.onFindAddressClick = function($that) {
				var $this = $that;
				this.geocodeAddress($this.val());
			}
		}).apply(mapField);

		$('.map-wrapper').each(function() {
			mapField.init($(this));
		});

		/* Add More Gallery Button */
		var files_count = $('#gallery-images-container .gallery-image').length;
		$('#add-more').click(function(e) {
			e.preventDefault();
			files_count++;
			$('#gallery-images-container').append('<div class="controls-holder"><input class="gallery-image image" name="gallery_image_' + files_count + '" type="file" /></div>');
		});

		/* Validate Image File Extension */
		function validateImage(file) {
			var validFileExtensions = ["jpg", "jpeg", "gif", "png", "pdf"];
			var ext = file.split('.').pop();
			if ($.inArray(ext, validFileExtensions) == -1) {
				return false;
			}
			return true;
		}

		/* Validate Submit Property Form */
		if (jQuery().validate) {
			jQuery.validator.addMethod("image", function(value, element) {
				return this.optional(element) || validateImage(value);
			}, "* Please provide image with proper extension! Only .jpg .gif and .png are allowed.");

			$('#submit-property-form').validate({
				rules : {
					bedrooms : {
						number : true
					},
					bathrooms : {
						number : true
					},
					garages : {
						number : true
					},
					price : {
						number : true
					},
					size : {
						number : true
					}
				}
			});

			/* Login , Register and Forgot Password Form */
			$('#login-form').validate();
			$('#register-form').validate();
			$('#forgot-form').validate();
		}

		/* Forgot Form */
		$('.login-register #forgot-form').slideUp('fast');
		$('.login-register .toggle-forgot-form').click(function(event) {
			event.preventDefault();
			$('.login-register #forgot-form').slideToggle('fast');
		});

		/* Edit Property Related Script */
		// Remove Featured Image
		$('a.remove-featured-image').click(function(event) {
			event.preventDefault();

			var $this = $(this);
			var gallery_thumb = $this.closest('.gallery-thumb');
			var loader = $this.siblings('.loader');

			loader.show();

			var removal_request = $.ajax({
				url : $this.attr('href'),
				type : "POST",
				data : {
					property_id : $this.data('property-id'),
					action : "remove_featured_image"
				},
				dataType : "html"
			});

			removal_request.done(function(msg) {
				var code = parseInt(msg);
				loader.hide();

				if (code == 3) {
					gallery_thumb.remove();
					$("#featured-file-container").removeClass('hidden');
				} else if (code == 2) {
					alert("Failed to remove!");
				} else if (code == 1) {
					alert("Invalide Parameters!");
				} else {
					alert("Unexpected response: " + msg);
				}

			});

			removal_request.fail(function(jqXHR, textStatus) {
				alert("Request failed: " + textStatus);
			});
		});

		// Remove Gallery Image
		$('a.remove-image').click(function(event) {
			event.preventDefault();

			var $this = $(this);
			var gallery_thumb = $this.closest('.gallery-thumb');
			var loader = $this.siblings('.loader');

			loader.show();

			var removal_request = $.ajax({
				url : $this.attr('href'),
				type : "POST",
				data : {
					property_id : $this.data('property-id'),
					gallery_img_id : $this.data('gallery-img-id'),
					action : "remove_gallery_image"
				},
				dataType : "html"
			});

			removal_request.done(function(msg) {
				var code = parseInt(msg);
				loader.hide();

				if (code == 3) {
					gallery_thumb.remove();
				} else if (code == 2) {
					alert("Failed to remove!");
				} else if (code == 1) {
					alert("Invalide Parameters!");
				} else {
					alert("Unexpected response: " + msg);
				}

			});

			removal_request.fail(function(jqXHR, textStatus) {
				alert("Request failed: " + textStatus);
			});
		});

		/* dsIDXpress */
		$('#dsidx-top-search #dsidx-search-form table td').removeClass('label');
		$('.dsidx-tag-pre-foreclosure br').replaceWith(' ');

		/*-----------------------------------------------------------------------------------*/
		/* Display Price Fields Based on Status Selection
		 /*-----------------------------------------------------------------------------------*/
		if ( typeof localized.rent_slug !== "undefined") {
			var property_status_changed = function(new_status) {
				var price_for_others = $('.advance-search-form .price-for-others');
				var price_for_rent = $('.advance-search-form .price-for-rent');
				if (price_for_others.length > 0 && price_for_rent.length > 0) {
					if (new_status == localized.rent_slug) {
						price_for_others.addClass('hide-fields').find('select').prop('disabled', true);
						price_for_rent.removeClass('hide-fields').find('select').prop('disabled', false);
					} else {
						price_for_rent.addClass('hide-fields').find('select').prop('disabled', true);
						price_for_others.removeClass('hide-fields').find('select').prop('disabled', false);
					}
				}
			}
			$('.advance-search-form #select-status').change(function(e) {
				var selected_status = $(this).val();
				property_status_changed(selected_status);
			});
			/* On page load ( as on search page ) */
			var selected_status = $('.advance-search-form #select-status').val();
			if (selected_status == localized.rent_slug) {
				property_status_changed(selected_status);
			}
		}

		/*-----------------------------------------------------------------------------------*/
		/* Properties Sorting
		 /*-----------------------------------------------------------------------------------*/
		function insertParam(key, value) {
			key = encodeURI(key);
			value = encodeURI(value);

			var kvp = document.location.search.substr(1).split('&');

			var i = kvp.length;
			var x;
			while (i--) {
				x = kvp[i].split('=');

				if (x[0] == key) {
					x[1] = value;
					kvp[i] = x.join('=');
					break;
				}
			}

			if (i < 0) {
				kvp[kvp.length] = [key, value].join('=');
			}

			//this will reload the page, it's likely better to store this until finished
			document.location.search = kvp.join('&');
		}

		$('#sort-properties').on('change', function() {
			var key = 'orderby';
			var value = $(this).val();
			insertParam(key, value);
		});

		/*-----------------------------------------------------------------------------------*/
		/* Modal dialog for Login and Register
		 /*-----------------------------------------------------------------------------------*/
		$('.activate-section').click(function(e) {
			e.preventDefault();
			var $this = $(this);
			var target_section = $this.data('section');
			$this.closest('.modal-section').hide();
			$this.closest('.forms-modal').find('.' + target_section).show();
		});

		/*-----------------------------------------------------------------------------------*/
		/* Add to favorites
		 /*-----------------------------------------------------------------------------------*/
		$('a#add-to-favorite').click(function(e) {
			e.preventDefault();
			var $star = $(this).find('i');
			var add_to_fav_opptions = {
				target : '#fav_target', // target element(s) to be updated with server response
				beforeSubmit : function() {
					$star.addClass('fa-spin');
				}, // pre-submit callback
				success : function() {
					$star.removeClass('fa-spin');
					$('#add-to-favorite').hide(0, function() {
						$('#fav_output').delay(200).show();
					});
				}
			};

			$('#add-to-favorite-form').ajaxSubmit(add_to_fav_opptions);
		});

		$(".socialbox").css("right", -230);
		$(".socialbox").hover(function() {
			$(".socialbox").css("right", 0);
		}, function() {
			$(".socialbox").css("right", -230);
		});
		var onetapped = true;
		$('.hover-tab').on('singletap', function(e) {
		    
		    if(onetapped){
		    	$(".socialbox").css("right", 0);
		    	onetapped = false;
		    	console.log("tap - " + onetapped);
		    } else {
		    	$(".socialbox").css("right", -230);
		    	onetapped = true;
		    	console.log("tap - " + onetapped);
		    }

		});
		
		// $('.hover-tab').on('tapend', function(e) { 
		    // $(".socialbox").css("right", -230);
		    // console.log("tap end");
		// });

		/*-----------------------------------------------------------------------------------*/
		/* Remove from favorites
		 /*-----------------------------------------------------------------------------------*/
		$('a.remove-from-favorite').click(function(event) {
			event.preventDefault();
			var $this = $(this);
			var property_item = $this.closest('.property-item');
			var loader = $this.siblings('.loader');
			var ajax_response = property_item.find('.ajax-response');

			$this.hide();
			loader.show();

			var remove_favorite_request = $.ajax({
				url : $this.attr('href'),
				type : "POST",
				data : {
					property_id : $this.data('property-id'),
					user_id : $this.data('user-id'),
					action : "remove_from_favorites"
				},
				dataType : "html"
			});

			remove_favorite_request.done(function(msg) {
				var code = parseInt(msg);
				loader.hide();

				if (code == 3) {
					property_item.remove();
				} else if (code == 2) {
					ajax_response.text("Failed to remove!");
				} else if (code == 1) {
					ajax_response.text("Invalide Parameters!");
				} else {
					ajax_response.text("Unexpected Response: " + msg);
				}

			});

			remove_favorite_request.fail(function(jqXHR, textStatus) {
				ajax_response.text("Request failed: " + textStatus);
			});
		});

	});
	/*
	 * pixelratio
	 */
})(jQuery);
