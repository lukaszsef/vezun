<?php
get_header();

// Banner Image
$banner_image_path = "";
$banner_image_id = get_post_meta($post -> ID, 'REAL_HOMES_page_banner_image', true);
if ($banner_image_id) {
	$banner_image_path = wp_get_attachment_url($banner_image_id);
} else {
	$banner_image_path = get_default_banner();
}
        ?>
<style>
	:root {
	  --plyr-color-main: #AB997B;
	}
</style>
        <div class="page-head" style="background-repeat: no-repeat;background-position: center top;background-image: url('<?php echo $banner_image_path; ?>'); background-size: cover;">
            <?php if(!('true' == get_option('theme_banner_titles'))): ?>
            <div class="container">
                <div class="wrap clearfix">
                    <h1 class="page-title"><span><?php _e('Property Details', 'framework'); ?></span></h1>
                    <p><?php the_title();
					
						
						
						if (!function_exists('display_parent_locations')) {
						function display_parent_locations($ct_trm) {
						if (!empty($ct_trm -> parent)) {
						$parent_location = get_term($ct_trm -> parent, 'property-city');
						echo ' - ' . $parent_location -> name;
						display_parent_locations($parent_location);
						// recursive call
						}
						}

						}

						/* Property City */
						// $city_terms = get_the_terms( $post->ID,"property-city" );
						// if(!empty($city_terms)){
						// foreach($city_terms as $ct_trm){
						// echo ' - '. $ct_trm->name;
						// display_parent_locations($ct_trm);
						// break;
						// }
						// }

						// $dzielnica_terms = get_the_terms( $post->ID,"property-dzielnica" );
						// if(!empty($dzielnica_terms)){
						// foreach($dzielnica_terms as $dzielnica_trm){
						// echo ' - '. $dzielnica_trm->name;
						// display_parent_locations($dzielnica_trm);
						// break;
						// }
						// }
						//
 ?>
                        </p>
                </div>
            </div>
            <?php endif; ?>
        </div><!-- End Page Head -->

		<?php
		if (have_posts()) :
		while (have_posts()) :
		the_post();
		?>
        
        <div class="container">
        	<div class="row">
        		<div class="col-sm-12">
					<?php get_template_part('property-details/property-gornypasek'); ?>	
        		</div>
        	</div>
        </div>
        <div class="container-fluid nopadding">
        	<div class="row nopadding">
        		<div class="col-sm-12 nopadding">
					<?php get_template_part('property-details/property-slider-new-2018'); ?>	
        		</div>
        	</div>
        </div>
        <div class="container">
        	<div class="row">
        		<div class="col-sm-12">
					<div class="share-property">
							<div class="d-md-flex justify-content-center align-items-center">
							<span>udostępnij ofertę</span> <a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" target="_blank"><i class="fa fa-facebook"></i></a><a href="https://twitter.com/share?url=<?php the_permalink(); ?>" target="_blank"><i class="fa fa-twitter"></i></a><a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink(); ?>&title=<?php the_title(); ?>&summary=<?php wp_title(); ?>&source=<?php the_permalink(); ?>" target="_blank"><i class="fa fa-linkedin"></i></a><a href="//pinterest.com/pin/create/link/?url=<?php the_permalink();?>&amp;description=<?php the_title();?>" target="_blank"><i class="fa fa-pinterest"></i></a><a href="mailto:?subject=<?php the_title(); ?>&amp;body=<?php wp_title(); ?> - <?php the_title(); ?>" target="_blank"><i class="fa fa-envelope"></i></a>
						</div>
					</div>
        		</div>
        	</div>
        	<?php if( get_field('youtube') ): ?>
	        	<div class="row">
	        		<div class="col-sm-12">
	        			<div class="video">
	        				<div id="playerVimeo" data-plyr-provider="vimeo" data-plyr-embed-id="<?php echo the_field('youtube') ?>"></div>
	        			</div>
	        		</div>
	        	</div>
        	<?php endif; ?>
        	<div class="row">
        		<div class="col-sm-12">
        			<div class="single-property-inner">
        				<div class="row">
        					<div class="col-sm-12 col-md-8">
        										<?php get_template_part('property-details/property-contents'); ?>
        					</div>
        					<div class="col-sm-12 col-md-4">
        						<?php get_template_part('property-details/property-agent-moje-2018'); ?>
        					</div>
        				</div>
        			</div>
        		</div>
        	</div>
        	<div class="row">
        		<div class="col-sm-12">
        			<?php if( get_field('spacer3d') ): ?>
					<article class="property-item spacer clearfix">
					<?php the_field('spacer3d') ?>
					</article>
					<?php endif; ?>
        		</div>
        	</div>
        	<div class="row">
        		<div class="col-sm-12">
        			<?php get_template_part('property-details/property-metas-single-2018'); ?>
        		</div>
        	</div>
        	<div class="row">
        		<div class="col-sm-12">
					<div id="overview">
						<?php get_template_part('property-details/property-map'); ?>
					</div>
				</div>
        	</div>
        	<?php get_template_part('property-details/similar-properties'); ?>

        </div>
		<?php 
			endwhile;
			endif;
        ?>
        
<?php get_footer(); ?>
<script>
            (function($) {
            	"use strict";
            	$(document).ready(function() {
            		
            		// $(".lightgallery").lightGallery({
            			// preload: 1,
            			// height: '100%'
            		// }); 
            		$('#imageGallery').lightGallery({
					    width: '100%',
					   // height: '800px',
					    mode: 'lg-fade',
					    addClass: 'fixed-size',
					    counter: false,
					    download: false,
					    startClass: 'fixedsize',
					    enableSwipe: true,
					    enableDrag: false,
					    speed: 500
					});
					
					$('.plany').lightGallery({
					    width: '100%',
					   // height: '800px',
					    mode: 'lg-fade',
					    addClass: 'fixed-size fixedsize-plany',
					    counter: false,
					    download: false,
					    startClass: 'fixedsize',
					    enableSwipe: true,
					    enableDrag: false,
					    speed: 500,
					    thumbnail: false
					});
					
					$('#imageGallery').lightSlider({
				        gallery:true,
				        item:1,
				        //adaptiveHeight:true,
				        loop:true,
				        thumbItem:9,
				        verticalHeight: 1000,
				        slideMargin:0,
				        enableDrag: true,
				        currentPagerPosition:'middle',
				        onSliderLoad: function(el) {
				            el.lightGallery({
				                selector: '#imageGallery .lslide'
				            });
				        }  
				    });  
					
            	});
            })(jQuery);
        </script>