<?php
/*
*   Template Name: Home Template
*/
get_header();
?>
<?php get_template_part('template-parts/slider'); ?>
<div class="container">
	<div class="row">
		<div class="col-sm-12">
			<div class="home-slogan">
				<h2>Nieruchomości szyte na miarę</h2>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12 col-md-6">
			<div class="home-block-image">
				<a href="//www.vezun.pl/property-status/sprzedaz/">
					<article>
						<div class="home-block-image-desc">
							<img src="//vezun.pl/wp-content/uploads/2020/10/perelka.jpg" alt="" class="img-fluid" />
							<span>Sprzedaż</span>
						</div>
					</article>
				</a>
			</div>
		</div>
		<div class="col-sm-12 col-md-6">
			<div class="home-block-image">
				<a href="//www.vezun.pl/property-status/wynajem/">
					<article>
						<div class="home-block-image-desc">
							<img src="//vezun.pl/wp-content/uploads/2020/10/wynajem.jpg" class="img-fluid" alt="" />
							<span>Wynajem</span>
						</div>
					</article>
				</a>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12 col-md-6">
			<div class="home-block-image">
				<a href="//www.vezun.pl/wyszukiwarka/?status=sprzedaz&type=perelki">
					<article>
						<div class="home-block-image-desc">
							<img src="//vezun.pl/wp-content/uploads/2020/10/sprzedaz.jpg" alt="" class="img-fluid" />
							<span>Perełki</span>
						</div>
					</article>
				</a>
			</div>
		</div>
		<div class="col-sm-12 col-md-6">
			<div class="home-block-image">
				<a href="/zagraniczne">
					<article>
						<div class="home-block-image-desc">
							<img src="//www.vezun.pl/wp-content/uploads/2018/05/Hiszpania.jpg" class="img-fluid" alt="" />
							<span>Zagraniczne</span>
						</div>
					</article>
				</a>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>