<?php
// Add Shortcode
function custom_shortcode( $atts , $content = null ) {

	// Code
return '<div id="single-langC" class="pl-lang lang-switch">'.$content.'</div>';
}
add_shortcode( 'pl', 'custom_shortcode' );

function custom_shortcode_en( $atts , $content = null ) {

	// Code
return '<div id="single-langC" class="en-lang lang-switch">'.$content.'</div>';
}
add_shortcode( 'en', 'custom_shortcode_en' );
?>