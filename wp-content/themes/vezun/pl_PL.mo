��    5      �  G   l      �     �     �     �     �  	   �     �     �     �  o   �     _     r          �     �  	   �     �     �  	   �     �  	   �     �  	   �  
   �     �     �     
                     5     M     V     \     m     }     �     �     �     �      �     �     �               ,     ;     J     d     k     �     �     �  m  �  -   
     >
     O
  	   W
  	   a
     k
     r
     w
  �   �
     A     O     ]     f     m     u     �     �     �     �     �     �     �     �     �            	   )     3  5   7     m  
   ~     �     �     �     �     �  	   �     �     �  (   �          %     9     K     ^     t     z     �     �     �     �     �               4   !          #                   2   0   3   1   "   -   	                  )          %      +               (       5       $                 '          
         *   ,                 &   .                 /                               Add in Homepage Slider All Property Cities Any Bathroom Bathrooms Bedroom Bedrooms Cena/m<sup>2</sup> Do you want to add this property in Homepage Slider ? If Yes, Then you also need to provide slider image below. Edit Property City Find Address First Garage Garages Know More Last Max Area Max Price Min Area Min Baths Min Beds Min Price Min Pricea Min and Max Area Min and Max Price More Details  Next No No Properties Found! Popular Property Cities Previous Print Property Address Property Cities Property City Property Details Property Dzielnica Property ID Property Location Property Location at Google Map* Property Panstwo Property Price Property Status Property Type Property Types Property Ulica Provide property address. Search Search Property Cities Select Property Cities Yes Yes  Project-Id-Version: RealHomes Theme
POT-Creation-Date: 2015-02-09 19:25+0100
PO-Revision-Date: 2015-02-12 11:21+0100
Last-Translator: Saqib <saqib@inspirythemes.com>
Language-Team: 
Language: en_US
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.7.1
X-Poedit-Basepath: .
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SearchPath-0: .
 Wyświetl nieruchomość na stronie głównej Wszystkie miasta Dowolne Łazienka Łazienki Pokój Pok. PLN/m<sup>2</sup> Czy chcesz dodać tę nieruchomość na Slider znajdujący się na stronie głównej ? Jeśli tak, to dodatkowo wskaż odpowiednie zdjęcie przyciskiem, który znajduje się poniżej. Edytuj miasta Szukaj adresu Pierwsza Garaż Garaże Czytaj więcej Ostatnia Powierzchnia do Cena do Powierzchnia od Min. liczba łazienek Ilość pokoi od Cena od Cena od Min. i max. powierzchnia Min. i max. cena Więcej informacji Następna Nie Nie znaleziono ofert spełniających podane kryteria. Popularne miasta Poprzednia Drukuj Adres nieruchomości Miasto Miasto Szczegóły nieruchomości Dzielnica Numer oferty Miejscowość Położenie nieruchomości na Google Map Państwo Cena nieruchomości Rodzaj transakcji Typ nieruchomości Rodzaj nieruchomości Ulica Wprowadz adres nieruchomości Szukaj Szukaj miasta Wybierz miasto Tak Tak 