<?php get_template_part("template-parts/carousel_partners"); ?>
<div class="newsletter">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="newsletter-inner">
					
					<p>Zapisz się do naszego newslettera.</p>
					<small>Otrzymuj powiadomienie o najnowszych i najciekawszych ofertach.</small>
					<div class="newsletter-form">
						<!-- <div><?php echo do_shortcode('[FM_form id="1"]'); ?></div> -->
						<!-- <div><?php echo do_shortcode('[fm_checkbox label="Label text"]'); ?></div> -->
						<!-- <input type="email" name="newsletter" id="newsletter" placeholder="Wpisz swój adres e-mail"/>
						<button>Wyślij</button> -->
						<?php echo do_shortcode('[contact-form-7 id="31098" title="newsletter"]'); ?>
					</div>
					<small>Korzystając ze strony zgadzasz się na używanie plików cookie, które są instalowane na Twoim urządzeniu. Za ich pomocą zbieramy informacje, które mogą stanowić dane osobowe. Wykorzystujemy je w celach analitycznych, marketingowych oraz aby dostosować treści do  Twoich preferencji i zainteresowań. Więcej o tym oraz o możliwościach zmiany ich ustawień dowiesz się w <a href="https://www.vezun.pl/wp-content/uploads/2018/05/Polityka-Prywatnosci.pdf" target="_blank">Polityce prywatności</a> i <a href="https://www.vezun.pl/wp-content/uploads/2018/05/Polityka-cookies-VEZUN.pdf" target="_blank">Polityce Cookies</a>.</small>
				</div>
			</div>
		</div>
	</div>
</div>
<footer id="footer-wrapper">
	<span id="skype_highlighting_settings" display="none" autoextractnumbers="0"></span>
	<div class="container">
		<div class="row mobile-text-center">
			<div class="col-sm-6 col-md-3">
				VEZUN PROPERTY
				<br />
				Private Real Estate Advisor			
			</div>
			<div class="col-sm-6 col-md-3">
				ul. Ojcowska 1
				<br />
				02-918 Warszawa
			</div>
			<div class="col-sm-6 col-md-3">
				Tel: +48&shy; 515&shy; 24 18 18 <br />
				Tel: +48&shy; 505&shy; 45 31 21 <br />
			</div>
			<div class="col-sm-6 col-md-3">
				Email: <a href="mailto:biuro@vezun.pl">biuro@vezun.pl</a>
			</div>
		</div>
	</div>
</footer>
<div class="copy-bottom-footer">
	<div class="container">
		<div class="copy-bottom-footer-inner">
			<div class="row">
				<div class="col-sm-12 col-md-6 mobile-text-center copy-text">
					Wszystkie prawa zastrzeżone &copy; Vezun Property
				</div>
				<div class="col-sm-12 col-md-6 text-right mobile-text-center">
					<a href="https://pl.linkedin.com/in/piotr-dzieniszewski-73960765" target="_blank"><i class="fa fa-linkedin"></i></a>
					<a href="https://www.facebook.com/vezunproperty/" target="_blank"><i class="fa fa-facebook"></i></a>
					<!-- <a href="#"><i class="fa fa-youtube"></i></a>
					<a href="#"><i class="fa fa-twitter"></i></a> -->
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<div class="author" style="text-align: right;font-size: 12px;color: #9c9c9c;font-family: 'Open Sans';">Wykonanie: <a href="http://www.webwave.pl" style="margin:0;font-size: 12px;    color: #0ecbe3;" target="_blank">WebWave.pl</a></div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>	
<?php wp_footer(); ?>
<div class="socialbox" style="right: -230px;display: none;">
			<div class="hover-tab"></div>
			<div class="social-content">
				<iframe src="//www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2Fpages%2FVezun-Property%2F441154155921453%3Ffref%3Dts&amp;width=240&amp;height=258&amp;colorscheme=dark&amp;show_faces=true&amp;header=false&amp;stream=false&amp;show_border=false" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:240px; height:258px;" allowtransparency="true"></iframe>
			</div>
		</div>
		<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PTSVC8R"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
</body>
</html>