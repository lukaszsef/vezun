<?php

$number_of_slides = intval(get_option('theme_number_of_slides'));
if(!$number_of_slides){
    $number_of_slides = -1;
}

$slider_args = array(
    'post_type' => 'property',
    'posts_per_page' => $number_of_slides,
    'meta_query' => array(
        array(
            'key' => 'REAL_HOMES_add_in_slider',
            'value' => 'yes',
            'compare' => 'LIKE'
        )
    )
);

$slider_query = new WP_Query( $slider_args );

if($slider_query->have_posts()){
    ?>
    <!-- Slider -->
    <div class="container-fluid">
    	<div class="row">
    		<div id="home-flexslider" class="clearfix">
    		    <div class="flexslider loading">
    		        <ul class="slides">
    		            <?php
    		            while ( $slider_query->have_posts() ) :
    		                $slider_query->the_post();
    		                $slider_image_id = get_post_meta( $post->ID, 'REAL_HOMES_slider_image', true );
    		                if($slider_image_id){
    		                    $slider_image_url = wp_get_attachment_url($slider_image_id);
    		                    ?>
    		                    <li>
    		                        <div class="desc-wrap">
    		                            <a href="<?php the_permalink(); ?>"><div class="slide-description">
    		                                <!-- <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3> -->
    		                                <!-- <p><?php framework_excerpt(15); ?></p> -->
    		                                <?php
    		                                
    												/*
    												 * Miasto
    												 */		
    												 
    												 
    												 		$type_terms = get_the_terms( $post->ID,"property-type" );
    										                $type_count = count($type_terms);
    										                if(!empty($type_terms)){
    										                    echo '<span> ';
    										                    $loop_count = 1;
    										                    foreach($type_terms as $typ_trm){
    										                        echo $typ_trm->name;
    										                        if($loop_count < $type_count && $type_count > 1){
    										                            echo ', ';
    										                        }
    										                        $loop_count++;
    										                    }
    										                    echo '&nbsp;/&nbsp;</span>';
    										                }else{
    										                    echo '&nbsp;';
    										                }
    												 		
    										                $type_terms = get_the_terms( $post->ID,"property-city" );
    										                $type_count = count($type_terms);
    										                if(!empty($type_terms)){
    										                    echo '<span> ';
    										                    $loop_count = 1;
    										                    foreach($type_terms as $typ_trm){
    										                        echo $typ_trm->name;
    										                        if($loop_count < $type_count && $type_count > 1){
    										                            echo ', ';
    										                        }
    										                        $loop_count++;
    										                    }
    										                    echo '</span>';
    										                }else{
    										                    echo '&nbsp;';
    										                }
    														
    														
    														
    														$price = get_property_price();
    		                                if ( $price ){
    		                                    echo '<span class="slider-prop-price">'."&nbsp;&nbsp;&nbsp;".$price.'</span>';
    		                                }
    		                                ?>
    		                                
    		                                <!-- <a href="<?php the_permalink(); ?>" class="know-more"><?php _e('Know More','framework'); ?></a> -->
    		                       </div></a>
    		                        </div>
    		                        <a href="<?php the_permalink(); ?>"><img class="lazy"  data-src="<?php echo $slider_image_url; ?>" alt="<?php the_title(); ?>"></a>
    		                    </li>
    		                    <?php
    		                }
    		            endwhile;
    		            wp_reset_query();
    		            ?>
    		        </ul>
    		    </div>
    		</div>
    	</div>
    </div>
    <!-- End Slider -->
    <?php
}else{
    get_template_part('banners/default_page_banner');
}
?>