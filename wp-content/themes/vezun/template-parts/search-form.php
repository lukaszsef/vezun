<?php
global $theme_search_fields;
if( !empty($theme_search_fields) ):
?>
<div class="as-form-wrap">
	<?php
	//print_r(array_values($theme_search_fields));
	//array_push($theme_search_fields, "dzielnica");
	// print_r(array_values($theme_search_fields));
	?>
    <form class="advance-search-form clearfix" action="<?php global $theme_search_url; echo $theme_search_url; ?>" method="get">
    <div class="row">

		<?php
    		if(in_array('status',$theme_search_fields)){
    	    ?>
    	    <div class="option-bar large col-sm-12 col-md-3">
    	        <label for="select-status"><?php _e('Property Status', 'framework'); ?></label>
    	        <span class="selectwrap">
    	            <select name="status" id="select-status" class="search-select">
    	                <?php advance_search_options('property-status'); ?>
    	            </select>
    	        </span>
    	    </div>
    	    <?php
		}
		if(in_array('wojewodztwo',$theme_search_fields)){
    	    ?>
    	    <div class="option-bar large col-sm-12 col-md-3">
    	        <label for="select-wojewodztwo"><?php _e('Województwo', 'framework'); ?></label>
    	        <span class="selectwrap">
    	            <select name="wojewodztwo" id="select-wojewodztwo" class="search-select" multiple>
    	                <?php advance_hierarchical_options('property-wojewodztwo'); ?>
    	            </select>
    	        </span>
    	    </div>
    	    <?php
    	}
    		if(in_array('location',$theme_search_fields)){
    	    ?>
    	    <div class="option-bar large col-sm-12 col-md-3">
    	        <label for="select-location"><?php _e('Property Location', 'framework'); ?></label>
    	        <span class="selectwrap">
    	            <select name="location[]" id="select-location" class="select-miasto" multiple="multiple">
    	                <?php advance_hierarchical_options('property-city'); ?>
    	            </select>
    	        </span>
    	    </div>
    	    <?php
    	}
    	if(in_array('dzielnica',$theme_search_fields)){
    	    ?>
    	    <div class="option-bar large col-sm-12 col-md-3">
    	        <label for="select-dzielnica"><?php _e('Property Dzielnica', 'framework'); ?></label>
    	        <span class="selectwrap">
    	            <select name="dzielnica[]" id="select-dzielnica" class="select-dzielnica" multiple="multiple">
    	                <?php advance_hierarchical_options('property-dzielnica'); ?>
    	            </select>
    	        </span>
    	    </div>
    	    <?php
		}
		
    		if(in_array('property-ulica',$theme_search_fields)){
    	    ?>
    	    <div class="option-bar large col-sm-12 col-md-3">
    	        <label for="property-ulica-txt"><?php _e('Property Ulica', 'framework'); ?></label>
    	        <input type="text" name="property-ulica" id="property-ulica-txt" value="<?php echo isset($_GET['property-ulica'])?$_GET['property-ulica']:''; ?>" placeholder="<?php _e('Any', 'framework'); ?>" />
    	    </div>
    	    <?php
    	}
    		if(in_array('type',$theme_search_fields)){
    	    ?>
    	    <div class="option-bar large col-sm-12 col-md-3">
    	        <label for="select-property-type"><?php _e('Property Type', 'framework'); ?></label>
    	        <span class="selectwrap">
    	            <select name="type" id="select-property-type" class="search-select">
    	                <?php advance_hierarchical_options('property-type'); ?>
    	            </select>
    	        </span>
    	    </div>
    	    <?php
    	}
    		if(in_array('rodzaj-budynku',$theme_search_fields)){
    	    ?>
    	    <div class="option-bar large col-sm-12 col-md-3">
    	        <label for="select-rodzaj-budynku"><?php _e('Rodzaj budynku', 'framework'); ?></label>
    	        <span class="selectwrap">
    	            <select name="rodzaj-budynku" id="select-rodzaj-budynku" class="search-select">
    	                <?php advance_hierarchical_options('property-rodzaj-budynku'); ?>
    	            </select>
    	        </span>
    	    </div>
    	    <?php
		}
		if(in_array('min-beds',$theme_search_fields)){
			?>
			<div class="option-bar large col-sm-12 col-md-3">
				<label for="select-bedrooms"><?php _e('Min Beds', 'framework'); ?></label>
				<span class="selectwrap">
					<select name="bedrooms" id="select-bedrooms" class="search-select">
						<?php numbers_list('bedrooms'); ?>
					</select>
				</span>
			</div>
			<?php
		}
    		if(in_array('min-max-area',$theme_search_fields)){
    	    $area_unit = get_option("theme_area_unit");
    	    ?>
    	    <div class="option-bar large col-sm-12 col-md-3">
    	        <label for="min-area"><?php _e('Min Area', 'framework'); ?> <span><?php if($area_unit){ echo "($area_unit)"; } ?></span></label>
    	        <input type="text" name="min-area" id="min-area" pattern="[0-9]+" value="<?php echo isset($_GET['min-area'])?$_GET['min-area']:''; ?>" placeholder="<?php _e('Any', 'framework'); ?>" title="<?php _e('Please only provide digits!','framework'); ?>" />
    	    </div>
    	
    	    <div class="option-bar large col-sm-12 col-md-3">
    	        <label for="max-area"><?php _e('Max Area', 'framework'); ?> <span><?php if($area_unit){ echo "($area_unit)"; } ?></span></label>
    	        <input type="text" name="max-area" id="max-area" pattern="[0-9]+" value="<?php echo isset($_GET['max-area'])?$_GET['max-area']:''; ?>" placeholder="<?php _e('Any', 'framework'); ?>" title="<?php _e('Please only provide digits!','framework'); ?>" />
    	    </div>
    	    <?php
		}
		if(in_array('min-max-price',$theme_search_fields)){
			?>
			
					
			
			<div class="option-bar large col-sm-12 col-md-3">
				<label for="min-price"><?php _e('Min Price', 'framework'); ?> </label>
				<input type="text" name="min-price" id="min-price" pattern="[0-9]+" value="<?php echo isset($_GET['min-price'])?$_GET['min-price']:''; ?>" placeholder="<?php _e('Any', 'framework'); ?>" title="<?php _e('Please only provide digits!','framework'); ?>" />
			</div>
		
			<div class="option-bar large col-sm-12 col-md-3">
				<label for="max-price"><?php _e('Max Price', 'framework'); ?> </label>
				<input type="text" name="max-price" id="max-price" pattern="[0-9]+" value="<?php echo isset($_GET['max-price'])?$_GET['max-price']:''; ?>" placeholder="<?php _e('Any', 'framework'); ?>" title="<?php _e('Please only provide digits!','framework'); ?>" />
			</div>
	<?php
		}
    		?>
    		<div class="col-sm-12">
    			<div class="search-carousel row">
					<?php 
    			if(in_array('panstwo',$theme_search_fields)){
    			    	    ?>
    			    	    <div class="option-bar large col-sm-12 col-md-3">
    			    	        <label for="select-panstwo"><?php _e('Property Panstwo', 'framework'); ?></label>
    			    	        <span class="selectwrap">
    			    	            <select name="panstwo" id="select-panstwo" class="search-select">
    			    	                <?php advance_hierarchical_options('property-panstwo'); ?>
    			    	            </select>
    			    	        </span>
    			    	    </div>
    			    	    <?php
    			    	}
    			    	if(in_array('min-baths',$theme_search_fields)){
    			    	    ?>
    			    	    <div class="option-bar large col-sm-12 col-md-3">
    			    	        <label for="select-bathrooms"><?php _e('Min Baths', 'framework'); ?></label>
    			    	        <span class="selectwrap">
    			    	            <select name="bathrooms" id="select-bathrooms" class="search-select">
    			    	                <?php numbers_list('bathrooms'); ?>
    			    	            </select>
    			    	        </span>
    			    	    </div>
    			    	    <?php
    			    	}
    			if(in_array('property-id',$theme_search_fields)){
    			    	    ?>
    			    	    <div class="option-bar large col-sm-12 col-md-3">
    			    	        <label for="property-id-txt"><?php _e('Property ID', 'framework'); ?></label>
    			    	        <input type="text" name="property-id" id="property-id-txt" value="<?php echo isset($_GET['property-id'])?$_GET['property-id']:''; ?>" placeholder="<?php _e('Any', 'framework'); ?>" />
    			    	    </div>
    			    	    <?php
						}
						if(in_array('doradca',$theme_search_fields)){
							?>
							<div class="option-bar large col-sm-12 col-md-3">
								<label for="select-doradca"><?php _e('Doradca', 'framework'); ?></label>
								<span class="selectwrap">
									<select name="doradca" id="select-doradca" class="search-select">
										<?php advance_search_options('property-doradca'); ?>
									</select>
								</span>
							</div>
							<?php
						}
						?>
    			</div>
    		</div>
    	<div class="col-sm-12">
    		<div class="option-bar search-button-right">
    				<a class="search-show-more">Wyszukiwanie zaawansowane</a>
    			    <input type="submit" value="<?php _e('Search', 'framework'); ?>" class=" real-btn btn">
    			</div>
    	</div>
    </div>
    </form>
</div>
<?php
endif;
?>