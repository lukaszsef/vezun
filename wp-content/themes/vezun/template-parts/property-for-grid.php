<div class="col-sm-12 col-md-6 col-lg-4">
	<article class="property-item clearfix">
	
	    <figure class="clearfix">
	        <a href="<?php the_permalink() ?>">
	            <?php
				global $post;
				if (has_post_thumbnail($post -> ID)) {
					the_post_thumbnail('grid-view-image');
				} else {
					inspiry_image_placeholder('grid-view-image');
				}
	            ?>
	        </a>
	
	        <?php display_figcaption($post -> ID); ?>
	
	    </figure>
		<?php $post_meta_data = get_post_custom($post -> ID); ?>
		<div class="grid-prop-szczegoly clearfix">
			<?php
							$price = get_property_price();
							$priceEU = get_property_price_eu();
							if ($price) { ?>
								
								<div class="cena">

									<?php 
									$panstwo = get_the_terms($post -> ID, "property-panstwo");
									if($panstwo[0]->name == 'Polska' || $panstwo == false) {
										echo $price;
										property_price_za_metr();
									}else {
										echo $priceEU;
										property_price_za_metr_eu();
									}?>

								</div>
								<?php 
								
								
								
							}
							
							?>
			<div class="row">
					<div class="col-sm-12">
						<div class="property-desc-top"><div class="miasto">
								<?php
								/*
								 * Miasto
								 */
								$type_terms = get_the_terms($post -> ID, "property-city");
								$type_count = count($type_terms);
								if (!empty($type_terms)) {
									$loop_count = 1;
									foreach ($type_terms as $typ_trm) {
										echo $typ_trm -> name;
										if ($loop_count < $type_count && $type_count > 1) {
											echo ', ';
										}
										$loop_count++;
									}
									echo '</span>';
								} else {
									echo '&nbsp;';
								}
								?>
							</div>
							<div class="dzielnica">
								<?php
								/*
								 * dzielnica
								 */
								$type_terms = get_the_terms($post -> ID, "property-dzielnica");
								$type_count = count($type_terms);
								if (!empty($type_terms)) {
									$loop_count = 1;
									foreach ($type_terms as $typ_trm) {
										echo $typ_trm -> name;
										if ($loop_count < $type_count && $type_count > 1) {
											echo ', ';
										}
										$loop_count++;
									}
									echo '</span>';
								} else {
									
								}
									?>
							</div>
							<div class="wojewodztwo">
								<?php
								/*
								 * wojewodztwo
								 */
								$type_terms = get_the_terms($post -> ID, "property-wojewodztwo");
								$type_count = count($type_terms);
								if (!empty($type_terms)) {
									$loop_count = 1;
									foreach ($type_terms as $typ_trm) {
										echo $typ_trm -> name;
										if ($loop_count < $type_count && $type_count > 1) {
											echo ', ';
										}
										$loop_count++;
									}
									echo '</span>';
								} else {
									
								}
									?>
							</div>
							<div class="ulica">
								<?php
								if (!empty($post_meta_data['REAL_HOMES_property_ulica'][0])) {
									$prop_ulica = $post_meta_data['REAL_HOMES_property_ulica'][0];
									if (!empty($post_meta_data['REAL_HOMES_property_ulica_postfix'][0])) {
										$prop_ulica_postfix = $post_meta_data['REAL_HOMES_property_ulica_postfix'][0];
										echo '&nbsp;' . $prop_ulica_postfix;
									}
									echo $prop_ulica;
									echo '</span>';
								}
								?>
							</div>
						</div>
					</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<div class="property-desc-bottom">
						<div class="metry">
							<?php
								if (!empty($post_meta_data['REAL_HOMES_property_size'][0])) {
									$prop_size = $post_meta_data['REAL_HOMES_property_size'][0];
									echo '<span><i class="icon-area"></i>';
									echo $prop_size;
									echo '&nbsp;m<sup>2</sup></span>';
								}
							?>
						</div>
						<div class="pokoje">
							<?php
								 if( !empty($post_meta_data['REAL_HOMES_property_bedrooms'][0]) ) {
									                $prop_bedrooms = floatval($post_meta_data['REAL_HOMES_property_bedrooms'][0]);
									                $bedrooms_label = ($prop_bedrooms > 1)? __('Bedrooms','framework' ): __('Bedroom','framework');
									                echo '<span><i class="icon-bed"></i>'. $prop_bedrooms .'&nbsp;'.$bedrooms_label.'</span>';
									       		}
							?>
						</div>
						<div class="garaze">
							<?php
								if( !empty($post_meta_data['REAL_HOMES_property_garage'][0]) ) {
									                $prop_garage = floatval($post_meta_data['REAL_HOMES_property_garage'][0]);
									                $garage_label = ($prop_garage > 1)?__('Garages','framework' ): __('Garage','framework');
									                echo '<span><i class="icon-garage"></i>'. $prop_garage .'&nbsp;'.$garage_label.'</span>';
									        	}
							?>
						</div>
					</div>
				</div>
			</div>
		</div>
	    <!-- <h4><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h4> -->
	    <!-- <p><?php framework_excerpt(10); ?> <a class="more-details" href="<?php the_permalink() ?>"><?php _e('More Details ','framework'); ?><i class="fa fa-caret-right"></i></a></p> -->
	    
	</article>
</div>