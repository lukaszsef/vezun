<!doctype html>
<!--[if lt IE 7]> <html class="lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]>    <html class="lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>    <html class="lt-ie9" <?php language_attributes(); ?>> <![endif]-->
	<!--[if gt IE 6]>    <html class="lt-ie" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?>> <!--<![endif]-->
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <title><?php wp_title('|', true, 'right'); ?><?php bloginfo('name'); ?></title>
    <?php
    $favicon = get_option('theme_favicon');
    if( !empty($favicon) )
    {
        ?>
        <link rel="shortcut icon" href="<?php echo $favicon; ?>" />
        <?php
		}
    ?>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="format-detection" content="telephone=no">
    <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    <link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?>" href="<?php bloginfo('rss2_url'); ?>" />
    <link rel="alternate" type="application/atom+xml" title="<?php bloginfo('name'); ?>" href="<?php bloginfo('atom_url'); ?>" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700&amp;subset=latin-ext" rel="stylesheet">
    <script src="https://player.vimeo.com/api/player.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <?php wp_head(); ?>
     <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-PTSVC8R');</script>
<!-- End Google Tag Manager -->
</head>
<body <?php body_class(); ?>>
	
	<div id="menu-toggle">
  <div id="menu-icon"> <span></span> <span></span> <span></span> <span></span> <span></span> <span></span> </div>
</div>
<div id="menu-overlay"></div>	
<ul id="menu">
	<?php wp_nav_menu(array('theme_location' => 'menu-mobile', 'menu_class' => 'clearfix', 'container_class' => 'fat-nav__wrapper')); ?>
</ul>	
<div id="page-content">
<div class="header-wrapper">
    <div class="container">
        <div class="d-flex flex-row justify-content-center top-menu">
        	<div class="d-flex align-items-end hidden-md-down">
        		<nav class="menu-top-left">
        			<?php wp_nav_menu(array('theme_location' => 'main-menu', 'menu_class' => 'clearfix')); ?>
        		</nav>
        	</div>
        	<div class="d-flex align-items-end flex-logo">
        		<div class="logo">
        			<a title="<?php  bloginfo('name'); ?>" href="<?php echo home_url(); ?>">
		                <img src="<?php bloginfo('template_directory'); ?>/images/logo.svg" alt="<?php  bloginfo('name'); ?>">
		            </a>
        		</div>
        	</div>
        	<div class="d-flex align-items-end hidden-md-down">
        		<nav class="menu-top-right">
        			<?php wp_nav_menu(array('theme_location' => 'second-menu', 'menu_class' => 'clearfix')); ?>
        		</nav>
        	</div>
        </div>
    </div>
</div>