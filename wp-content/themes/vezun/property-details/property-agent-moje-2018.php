<?php
$display_agent_info = get_option('theme_display_agent_info');
$agent_display_option = get_post_meta($post->ID, 'REAL_HOMES_agent_display_option',true);
if($display_agent_info == 'true'){
    $property_title = get_the_title($post->ID);
    $property_permalink = get_permalink($post->ID);

    $display_author = false; // flag to display author info instead of agent info
    $hide_info_box = true;

    $agent_id = null;
    $agent_mobile = null;
    $agent_office_phone = null;
    $agent_office_fax = null;
    $agent_email = null;
    $agent_title_text = null;
    $agent_description = null;

    if($agent_display_option == "my_profile_info"){
        $display_author = true;
        $hide_info_box = false;

        $agent_mobile = get_the_author_meta('mobile_number');
        $agent_office_phone = get_the_author_meta('office_number');
        $agent_office_fax = get_the_author_meta('fax_number');
        $agent_email = get_the_author_meta('user_email');

        $agent_title_text = __('Submitted by','framework')." ".get_the_author_meta('display_name');

    }else{
        $property_agent = get_post_meta($post->ID, 'REAL_HOMES_agents',true);
        if( ( !empty($property_agent) ) && ( intval($property_agent) > 0 ) ){
            $hide_info_box = false;

            $agent_id = intval( $property_agent );

            $agent_mobile = get_post_meta($agent_id, 'REAL_HOMES_mobile_number',true);
            $agent_office_phone = get_post_meta($agent_id, 'REAL_HOMES_office_number',true);
            $agent_office_fax = get_post_meta($agent_id, 'REAL_HOMES_fax_number',true);
            $agent_email = get_post_meta($agent_id, 'REAL_HOMES_agent_email',true);

            $agent_title_text = __('Agent','framework')." ".get_the_title($agent_id);

            $agent_excerpt = get_post_field('post_content', $agent_id);
            $agent_description = get_framework_custom_excerpt( $agent_excerpt );

        }
    }
    if( !$hide_info_box ){
        ?>
        <div class="agent-detail clearfix">

            <div class="left-box">
                <?php
                if(has_post_thumbnail($agent_id)){
                        ?><figure><?php echo get_the_post_thumbnail( $agent_id, 'agent-image'); ?></figure><?php
                    }
                ?>
                <?php echo $agent_description;  ?>
            </div>
        </div>
        <?php
    } else { ?>
    	<div class="agent-detail clearfix">

            <div class="left-box">
                <figure><img width="310" height="470" src="https://www.vezun.pl/wp-content/uploads/2015/02/Piotr-310x470.jpg" class="attachment-agent-image size-agent-image wp-post-image" alt=""></figure>                <div class="info-contact-inner">
        				<span>Kontakt do Doradcy:</span><br>Piotr Dzieniszewski<br>tel: +48 515 24 18 18<br><a href="mailto:biuro@vezun.pl">biuro@vezun.pl</a>
        			</div>            </div>
        </div>
    <?php }
}
?>