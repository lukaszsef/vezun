<?php
$size = 'full';
$properties_images = rwmb_meta( 'REAL_HOMES_property_images', 'type=plupload_image&size='.$size, $post->ID );
// $properties_images_for_print = rwmb_meta( 'REAL_HOMES_property_images_for_print', $post->ID );
$size = 'property-detail-slider-image-two';
?>
<div  id="property-slider-two-wrapper">
        <div id="property-slider-two" class="lSSlideOuter">
            <ul id="imageGallery" class="gallery list-unstyled lightSlider lSSlide">
                <?php
                foreach( $properties_images as $prop_image_id=>$prop_image_meta ){
                	$slider_thumb_wieksze = wp_get_attachment_image_src($prop_image_id, 'property-thumb-image-wieksze');
					$slider_thumb = wp_get_attachment_image_src($prop_image_id, 'property-thumb-image');
                    echo '<li data-thumb="' . $slider_thumb[0] . '" data-src="'.$prop_image_meta['full_url'].'">';
					//echo '<a href="'.$prop_image_meta['full_url'].'" class="'.get_lightbox_plugin_class() .'" '.generate_gallery_attribute().'>';
                    //echo '<a href="'.$prop_image_meta['full_url'].'" class="lightbox-responsive" rel="lightbox[gallery-1]" '.generate_gallery_attribute().'>';
                    //echo '<a href="'.$prop_image_meta['full_url'].'" class="lightbox-responsive" rel="lightbox[gallery-1]" data-lightbox-gallery="lightbox[gallery-1]">';
                    echo '<img src="'.$prop_image_meta['url'].'" alt="'.$prop_image_meta['title'].'" />';
                    //echo '</a>';
                    echo '</li>';
                }
                ?>
            </ul>
        </div>
    </div>
    <div id="imagePrint" style="display: none;">
    	<?php the_post_thumbnail( 'large','style=max-width:100%;height:auto;'); ?>
    </div>