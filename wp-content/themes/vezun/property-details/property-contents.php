<?php
$post_meta_data = get_post_custom($post -> ID);
?>


<div class="row">
	<div class="col-sm-12">
		<div id="property-descriptions"><?php lang_switcher(); ?><?php
        	the_content();?>
        	<!-- <?php if (!empty($post_meta_data['REAL_HOMES_property_fotograf'][0])) {
        echo "<div class=autor-zdjec>Autorem zdjęć jest: ";
			$prop_fotograf = $post_meta_data['REAL_HOMES_property_fotograf'][0];
			echo $prop_fotograf . "</div>";
		}?> -->
		<small class="copyPrivacy">Przedstawiona wyżej oferta nie jest ofertą handlową w rozumieniu przepisów prawa lecz ma charakter informacyjny. Firma VEZUN PROPERTY dokłada wszelkich starań, aby treści przedstawione w naszych ofertach były rzetelne i aktualne. Dane dotyczące ofert uzyskano na podstawie oświadczeń Sprzedających.</small>
        </div>
	</div>
</div>