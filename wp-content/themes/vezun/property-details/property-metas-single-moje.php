<?php
$post_meta_data = get_post_custom($post -> ID);
?>
<table class="tg">
<tr class="numer-oferty-table">
<td class="requirementRight">Numer oferty</td>
<td class="tg-031e"><?php

/* Property ID if exists */
$property_id = get_post_meta($post -> ID, 'REAL_HOMES_property_id', true);
if (!empty($property_id)) {
	echo $property_id;
}
            ?></td>
</tr>
<tr>
<td class="requirementRight">Cena:</td>
<td class="tg-031e"><?php  property_price(); ?></td>
</tr>
<tr>
<td class="requirementRight">Cena/m<sup>2</sup>:</td>
<td class="tg-031e"><?php property_price_za_metr(); ?></td>
</tr>
<tr>
	</table>
	<hr />
	<table class="tg">
<td class="requirementRight">Miasto:</td>
<td class="tg-031e"><?php /*
	 * Miasto
	 */
	$type_terms = get_the_terms($post -> ID, "property-city");
	$type_count = count($type_terms);
	if (!empty($type_terms)) {
		$loop_count = 1;
		foreach ($type_terms as $typ_trm) {
			echo $typ_trm -> name;
			if ($loop_count < $type_count && $type_count > 1) {
				echo ', ';
			}
			$loop_count++;
		}
		echo '</span>';
	} else {
		echo '&nbsp;';
	}
 ?></td>
</tr>
<tr>
<td class="requirementRight">Dzielnica:</td>
<td class="tg-031e"><?php
	$type_terms = get_the_terms($post -> ID, "property-dzielnica");
	$type_count = count($type_terms);
	if (!empty($type_terms)) {
		$loop_count = 1;
		foreach ($type_terms as $typ_trm) {
			echo $typ_trm -> name;
			if ($loop_count < $type_count && $type_count > 1) {
				echo ', ';
			}
			$loop_count++;
		}
		echo '</span>';
	}
 ?></td>
</tr>
<tr>
<td class="requirementRight">Województwo:</td>
<td class="tg-031e"><?php
	$type_terms = get_the_terms($post -> ID, "property-wojewodztwo");
	$type_count = count($type_terms);
	if (!empty($type_terms)) {
		$loop_count = 1;
		foreach ($type_terms as $typ_trm) {
			echo $typ_trm -> name;
			if ($loop_count < $type_count && $type_count > 1) {
				echo ', ';
			}
			$loop_count++;
		}
		echo '</span>';
	}
 ?></td>
</tr>
<tr>
<td class="requirementRight">Ulica:</td>
<td class="tg-031e"><?php
	if (!empty($post_meta_data['REAL_HOMES_property_ulica'][0])) {
		$prop_ulica = $post_meta_data['REAL_HOMES_property_ulica'][0];
		if (!empty($post_meta_data['REAL_HOMES_property_ulica_postfix'][0])) {
			$prop_ulica_postfix = $post_meta_data['REAL_HOMES_property_ulica_postfix'][0];
			echo '&nbsp;' . $prop_ulica_postfix;
		}
		echo $prop_ulica;
		echo '</span>';
	}
 ?></td>
</tr>
</table>
<hr />
<table class="tg">
<tr>
<td class="requirementRight">Powierzchnia:</td>
<td class="tg-031e"><?php
	if (!empty($post_meta_data['REAL_HOMES_property_size'][0])) {
		$prop_size = $post_meta_data['REAL_HOMES_property_size'][0];
		echo $prop_size;
		echo '&nbsp;m<sup>2</sup></span>';
	}
 ?></td>
</tr>
<tr>
<td class="requirementRight">Ilość pokoi:</td>
<td class="tg-031e"><?php
	if (!empty($post_meta_data['REAL_HOMES_property_bedrooms'][0])) {
		$prop_bedrooms = floatval($post_meta_data['REAL_HOMES_property_bedrooms'][0]);
		$bedrooms_label = ($prop_bedrooms > 1) ? __('Bedrooms', 'framework') : __('Bedroom', 'framework');
		echo $prop_bedrooms . '&nbsp;' . $bedrooms_label . '</span>';
	}
 ?></td>
</tr>
<tr>
<td class="requirementRight">Ilość łazienek:</td>
<td class="tg-031e"><?php
	if (!empty($post_meta_data['REAL_HOMES_property_bathrooms'][0])) {
		$prop_bathrooms = floatval($post_meta_data['REAL_HOMES_property_bathrooms'][0]);
		$bathrooms_label = ($prop_bathrooms > 1) ? __('Bathrooms', 'framework') : __('Bathroom', 'framework');
		echo $prop_bathrooms . '</span>';
	}
 ?></td>
</tr>
<tr>
<td class="requirementRight">Balkon:</td>
<td class="tg-031e"><?php
if( !empty($post_meta_data['REAL_HOMES_property_balkon'][0]) ) {
$prop_balkon = $post_meta_data['REAL_HOMES_property_balkon'][0];
echo $prop_balkon;
} ?></td>
</tr>
<tr>
<td class="requirementRight">Taras:</td>
<td class="tg-031e"><?php
if( !empty($post_meta_data['REAL_HOMES_property_taras'][0]) ) {
$prop_taras = $post_meta_data['REAL_HOMES_property_taras'][0];
echo $prop_taras;
} ?></td>
</tr>
<tr>
<td class="requirementRight">Garaż:</td>
<td class="tg-031e"><?php
if( !empty($post_meta_data['REAL_HOMES_property_garage'][0]) ) {
$prop_garage = $post_meta_data['REAL_HOMES_property_garage'][0];
echo $prop_garage;
} ?></td>
</tr>
<tr>
<td class="requirementRight">Komórka lokatorska:</td>
<td class="tg-031e"><?php
if( !empty($post_meta_data['REAL_HOMES_property_komorka'][0]) ) {
$prop_komorka = $post_meta_data['REAL_HOMES_property_komorka'][0];
echo $prop_komorka;
} ?></td>
</tr>
</table>
<hr />
<table class="tg">
<tr>
<td class="requirementRight">Budynek:</td>
<td class="tg-031e"><?php /*
	 * Rodzaj budynku
	 */
	$type_terms = get_the_terms($post -> ID, "property-rodzaj-budynku");
	$type_count = count($type_terms);
	if (!empty($type_terms)) {
		$loop_count = 1;
		foreach ($type_terms as $typ_trm) {
			echo $typ_trm -> name;
			if ($loop_count < $type_count && $type_count > 1) {
				echo ', ';
			}
			$loop_count++;
		}
		echo '</span>';
	} else {
		echo '&nbsp;';
	}
?></td>
</tr>
<tr>
<td class="requirementRight">Pow. działki:</td>
<td class="tg-031e"><?php
if( !empty($post_meta_data['REAL_HOMES_property_pow_dzialki'][0]) ) {
$pow_dzialki = $post_meta_data['REAL_HOMES_property_pow_dzialki'][0];
echo $pow_dzialki;
} ?></td>
</tr>
<tr>
<td class="requirementRight">Piętro:</td>
<td class="tg-031e"><?php
if( !empty($post_meta_data['REAL_HOMES_property_pietro'][0]) ) {
$prop_pietro = $post_meta_data['REAL_HOMES_property_pietro'][0];
echo $prop_pietro;
} ?></td>
</tr>
<tr>
<td class="requirementRight">Winda:</td>
<td class="tg-031e"><?php
if( !empty($post_meta_data['REAL_HOMES_property_winda'][0]) ) {
$prop_winda = $post_meta_data['REAL_HOMES_property_winda'][0];
echo $prop_winda;
} ?></td>
</tr>
</table>
<hr />
<table class="tg">
<tr>
<td class="requirementRight">Czynsz:</td>
<td class="tg-031e czynsz-tabela"><?php
if( !empty($post_meta_data['REAL_HOMES_property_czynsz'][0]) ) {
$prop_czynsz = number_format($post_meta_data['REAL_HOMES_property_czynsz'][0], 0, ' ', ' ');
echo $prop_czynsz . " PLN";
} 

?></td>
</tr>
<tr>
<td class="requirementRight">Przeznaczenie:</td>
<td class="tg-031e"><?php
if( !empty($post_meta_data['REAL_HOMES_property_przeznaczenie'][0]) ) {
$prop_przeznaczenie = $post_meta_data['REAL_HOMES_property_przeznaczenie'][0];
echo $prop_przeznaczenie;
} ?></td>
</tr>
<tr>
<td class="requirementRight">Własność gruntu:</td>
<td class="tg-031e"><?php
if( !empty($post_meta_data['REAL_HOMES_property_wlasnosc_gruntu'][0]) ) {
$prop_wlasnosc_gruntu = $post_meta_data['REAL_HOMES_property_wlasnosc_gruntu'][0];
echo $prop_wlasnosc_gruntu;
} ?></td>
</tr>
<tr>
<td class="requirementRight">Media:</td>
<td class="tg-031e"><?php
if( !empty($post_meta_data['REAL_HOMES_property_media'][0]) ) {
$prop_media = $post_meta_data['REAL_HOMES_property_media'][0];
echo $prop_media;
} ?></td>
</tr>

<tr>
<td class="requirementRight">Ekspozycja okienna:</td>
<td class="tg-031e"><?php
if( !empty($post_meta_data['REAL_HOMES_property_eksp_okienna'][0]) ) {
$prop_eksp_okienna = $post_meta_data['REAL_HOMES_property_eksp_okienna'][0];
echo $prop_eksp_okienna;
} ?></td>
</tr>
</table>
<br /><br />
<table class="tg">
<tr>
<td class="requirementRight">Kontakt:</td>
<td class="tg-031e">&nbsp;</td>
</tr>
<tr>
<td class="requirementRight">Piotr Dzieniszewski</td>
<td class="tg-031e">tel: 515 24 18 18</td>
</tr>
<tr>
<td class="requirementRight">Sylwia Dzieniszewska</td>
<td class="tg-031e">tel: 505 45 31 21</td>
</tr>
</table>