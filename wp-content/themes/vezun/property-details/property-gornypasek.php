<div class="property-meta property-meta-gorny-pasek clearfix">
       <?php
        $post_meta_data = get_post_custom($post->ID);

        
		
		/*
		 * stare z postfixem
		 * 
		 */
		 // if( !empty($post_meta_data['REAL_HOMES_property_size'][0]) ) {
                // $prop_size = $post_meta_data['REAL_HOMES_property_size'][0];
                // echo '<span><i class="icon-area"></i>';
                // echo $prop_size;
                // if( !empty($post_meta_data['REAL_HOMES_property_size_postfix'][0]) ){
                    // $prop_size_postfix = $post_meta_data['REAL_HOMES_property_size_postfix'][0];
                    // echo '&nbsp;'.$prop_size_postfix;
                // }
                // echo '</span>';
        // }
		 /*
		  * end
		  */
		  
        echo "<span class=gora-price>";
          

        $panstwo = get_the_terms($post -> ID, "property-panstwo");
        if($panstwo[0]->name == 'Polska' || $panstwo == false) {
            property_price() . property_price_za_metr();
        }else {
            property_price_eu() . property_price_za_metr_eu();
        }

                     
        echo "</span>";
		
		/*
		 * city
		 */
                $type_terms = get_the_terms( $post->ID,"property-city" );
                $type_count = count($type_terms);
                if(!empty($type_terms)){
                    echo '<span class="miasto-gora pad">';
                    $loop_count = 1;
                    foreach($type_terms as $typ_trm){
                        echo $typ_trm->name;
                        if($loop_count < $type_count && $type_count > 1){
                            echo ', ';
                        }
                        $loop_count++;
                    }
                    echo '</span>';
                }else{
                    echo '&nbsp;';
                }

	/*
		 * dzielnica
		 */
                $type_terms = get_the_terms( $post->ID,"property-dzielnica" );
                $type_count = count($type_terms);
                if(!empty($type_terms)){
                    echo '<span class="dzielnica-gora pad">';
                    $loop_count = 1;
                    foreach($type_terms as $typ_trm){
                        echo $typ_trm->name;
                        if($loop_count < $type_count && $type_count > 1){
                            echo ', ';
                        }
                        $loop_count++;
                    }
                    echo '</span>';
                }else{
                    echo '&nbsp;';
                }

        /*
		 * wojewodztwo
		 */
        $type_terms = get_the_terms( $post->ID,"property-wojewodztwo" );
        $type_count = count($type_terms);
        if(!empty($type_terms)){
            echo '<span class="wojewodztwo-gora pad">';
            $loop_count = 1;
            foreach($type_terms as $typ_trm){
                echo $typ_trm->name;
                if($loop_count < $type_count && $type_count > 1){
                    echo ', ';
                }
                $loop_count++;
            }
            echo '</span>';
        }else{
            echo '&nbsp;';
        }
		if( !empty($post_meta_data['REAL_HOMES_property_ulica'][0]) ) {
                $prop_ulica = $post_meta_data['REAL_HOMES_property_ulica'][0];
                echo '<span class="ulica-gora pad">';
                if( !empty($post_meta_data['REAL_HOMES_property_ulica_postfix'][0]) ){
                    $prop_ulica_postfix = $post_meta_data['REAL_HOMES_property_ulica_postfix'][0];
                    echo '&nbsp;'.$prop_ulica_postfix;
                }
				echo $prop_ulica;
                echo '</span>';
        }
if( !empty($post_meta_data['REAL_HOMES_property_size'][0]) ) {
                $prop_size = $post_meta_data['REAL_HOMES_property_size'][0];
                echo '<span class="size-gora pad"><i class="icon-area"></i>';
                echo $prop_size;
                echo '&nbsp;m<sup>2</sup></span>';
        }
        if( !empty($post_meta_data['REAL_HOMES_property_bedrooms'][0]) ) {
                $prop_bedrooms = floatval($post_meta_data['REAL_HOMES_property_bedrooms'][0]);
                $bedrooms_label = ($prop_bedrooms > 1)? __('Bedrooms','framework' ): __('Bedroom','framework');
                echo '<span class="pokoje-gora pad"><i class="icon-bed"></i>'. $prop_bedrooms .'&nbsp;'.$bedrooms_label.'</span>';
        }
		if( !empty($post_meta_data['REAL_HOMES_property_garage'][0]) ) {
                $prop_garage = floatval($post_meta_data['REAL_HOMES_property_garage'][0]);
                $garage_label = ($prop_garage > 1)?__('Garages','framework' ): __('Garage','framework');
                echo '<span class="garage-gora pad"><i class="icon-garage"></i>'. $prop_garage .'&nbsp;'.$garage_label.'</span>';
        }
		// if( !empty($post_meta_data['REAL_HOMES_property_okna'][0]) ) {
                // $prop_okna = $post_meta_data['REAL_HOMES_property_okna'][0];
                // echo '<span class="garage-gora pad"><i class="icon-garage"></i>'. $prop_okna;
        // }
		
		
		
        // if( !empty($post_meta_data['REAL_HOMES_property_bathrooms'][0]) ) {
                // $prop_bathrooms = floatval($post_meta_data['REAL_HOMES_property_bathrooms'][0]);
                // $bathrooms_label = ($prop_bathrooms > 1)?__('Bathrooms','framework' ): __('Bathroom','framework');
                // echo '<span><i class="icon-bath"></i>'. $prop_bathrooms .'&nbsp;'.$bathrooms_label.'</span>';
        // }
		// if( !empty($post_meta_data['REAL_HOMES_property_cena_za_metr'][0]) ) {
                // $prop_cena_za_metr = floatval($post_meta_data['REAL_HOMES_property_cena_za_metr'][0]);
                // $cena_za_metr_label = ($prop_cena_za_metr > 1)?__('Cena/m<sup>2</sup>','framework' ): __('Cena/m<sup>2</sup>','framework');
                // echo '<span><i class="icon-price"></i>'.'<span class="cena-metr">' . $prop_cena_za_metr . '</span>' . '&nbsp;'.$cena_za_metr_label.'</span>';
        // }
		// if( !empty($post_meta_data['REAL_HOMES_property_ulica'][0]) ) {
                // $prop_ulica = ($post_meta_data['REAL_HOMES_property_ulica'][0]);
                // $ulica_label = ($prop_ulica > 1)?__('ul.','framework' ): __('ul.','framework');
                // echo '<span><i class="icon-street"></i>'. $ulica_label .'&nbsp;'.$prop_ulica.'</span>';
        // }

		

        
		/*
		 * dzielnica
		 */
                // $type_terms = get_the_terms( $post->ID,"property-dzielnica" );
                // $type_count = count($type_terms);
                // if(!empty($type_terms)){
                    // echo '<span><i class="icon-dzielnica"></i>';
                    // $loop_count = 1;
                    // foreach($type_terms as $typ_trm){
                        // echo $typ_trm->name;
                        // if($loop_count < $type_count && $type_count > 1){
                            // echo ', ';
                        // }
                        // $loop_count++;
                    // }
                    // echo '</span>';
                // }else{
                    // echo '&nbsp;';
                // }
		/*
		 * city
		 */
                // $type_terms = get_the_terms( $post->ID,"property-city" );
                // $type_count = count($type_terms);
                // if(!empty($type_terms)){
                    // echo '<span><i class="icon-town"></i>';
                    // $loop_count = 1;
                    // foreach($type_terms as $typ_trm){
                        // echo $typ_trm->name;
                        // if($loop_count < $type_count && $type_count > 1){
                            // echo ', ';
                        // }
                        // $loop_count++;
                    // }
                    // echo '</span>';
                // }else{
                    // echo '&nbsp;';
                // }
		/*
		 * Rodzaj budynku
		 */
                // $type_terms = get_the_terms( $post->ID,"property-rodzaj-budynku" );
                // $type_count = count($type_terms);
                // if(!empty($type_terms)){
                    // echo '<span><i class="icon-buildings"></i>';
                    // $loop_count = 1;
                    // foreach($type_terms as $typ_trm){
                        // echo $typ_trm->name;
                        // if($loop_count < $type_count && $type_count > 1){
                            // echo ', ';
                        // }
                        // $loop_count++;
                    // }
                    // echo '</span>';
                // }else{
                    // echo '&nbsp;';
                // }
  ?>

      
</div>