<?php
$post_meta_data = get_post_custom($post -> ID);
?>
<div class="meta-container">
	<div class="row">
		<div class="col-md-9">
			<div class="row">
				
				<div class="col-md-6 col-lg-5">
					<div class="row">
						<div class="single-meta-info col-md-12 <?php $type_terms = get_the_terms($post -> ID, "property-city"); if (empty($type_terms)) echo "display-none"; ?>">
							<?php /*
						 * Miasto
						 */
						$type_terms = get_the_terms($post -> ID, "property-city");
						$type_count = count($type_terms);
						if (!empty($type_terms)) {
							echo "<span>Miasto: </span>";
							$loop_count = 1;
							foreach ($type_terms as $typ_trm) {
								echo $typ_trm -> name;
								if ($loop_count < $type_count && $type_count > 1) {
									echo ', ';
								}
								$loop_count++;
							}
							echo '</span>';
						} else {
							echo '&nbsp;';
						}
						 ?>
						</div>
						<div class="single-meta-info col-md-12 <?php $type_terms = get_the_terms($post -> ID, "property-dzielnica"); if (empty($type_terms)) echo "display-none"; ?>">
							<?php
						$type_terms = get_the_terms($post -> ID, "property-dzielnica");
						$type_count = count($type_terms);
						if (!empty($type_terms)) {
							echo "<span>Dzielnica: </span>";
							$loop_count = 1;
							foreach ($type_terms as $typ_trm) {
								echo $typ_trm -> name;
								if ($loop_count < $type_count && $type_count > 1) {
									echo ', ';
								}
								$loop_count++;
							}
							echo '</span>';
						}
						 ?>
						</div>
						<div class="single-meta-info col-md-12 <?php $type_terms = get_the_terms($post -> ID, "property-wojewodztwo"); if (empty($type_terms)) echo "display-none"; ?>">
							<?php
						$type_terms = get_the_terms($post -> ID, "property-wojewodztwo");
						$type_count = count($type_terms);
						if (!empty($type_terms)) {
							echo "<span>Województwo: </span>";
							$loop_count = 1;
							foreach ($type_terms as $typ_trm) {
								echo $typ_trm -> name;
								if ($loop_count < $type_count && $type_count > 1) {
									echo ', ';
								}
								$loop_count++;
							}
							echo '</span>';
						}
						 ?>
						</div>
						<div class="single-meta-info col-md-12 <?php if (empty($post_meta_data['REAL_HOMES_property_ulica'][0])) echo "display-none" ?>">
							<?php
							if (!empty($post_meta_data['REAL_HOMES_property_ulica'][0])) {
								echo "<span>Ulica: </span>";
								$prop_ulica = $post_meta_data['REAL_HOMES_property_ulica'][0];
								if (!empty($post_meta_data['REAL_HOMES_property_ulica_postfix'][0])) {
									$prop_ulica_postfix = $post_meta_data['REAL_HOMES_property_ulica_postfix'][0];
									echo '&nbsp;' . $prop_ulica_postfix;
								}
								echo $prop_ulica;
								echo '</span>';
							}
							?>
						 </div>
						 <div class="single-meta-info col-md-12 <?php if (empty($post_meta_data['REAL_HOMES_property_size'][0])) echo "display-none" ?>">
						 	<?php
						if (!empty($post_meta_data['REAL_HOMES_property_size'][0])) {
							echo "<span>Powierzchnia: </span>";
							$prop_size = $post_meta_data['REAL_HOMES_property_size'][0];
							echo $prop_size;
							echo '&nbsp;m<sup>2</sup></span>';
						}
						 ?>
						 </div>
						 <div class="single-meta-info col-md-12 <?php if (empty($post_meta_data['REAL_HOMES_property_bedrooms'][0])) echo "display-none" ?>">
						 	<?php
			if (!empty($post_meta_data['REAL_HOMES_property_bedrooms'][0])) {
				echo "<span>Ilość pokoi: </span>";
				$prop_bedrooms = floatval($post_meta_data['REAL_HOMES_property_bedrooms'][0]);
				$bedrooms_label = ($prop_bedrooms > 1) ? __('Bedrooms', 'framework') : __('Bedroom', 'framework');
				echo $prop_bedrooms . '&nbsp;' . $bedrooms_label . '</span>';
			}
		 ?>
						 </div>
						 <div class="single-meta-info col-md-12 <?php if (empty($post_meta_data['REAL_HOMES_property_bathrooms'][0])) echo "display-none" ?>">
							<?php
							if (!empty($post_meta_data['REAL_HOMES_property_bathrooms'][0])) {
								echo "<span>Ilość łazienek: </span>";
								$prop_bathrooms = floatval($post_meta_data['REAL_HOMES_property_bathrooms'][0]);
								$bathrooms_label = ($prop_bathrooms > 1) ? __('Bathrooms', 'framework') : __('Bathroom', 'framework');
								echo $prop_bathrooms . '</span>';
							}
							?>
						 </div>
						 <div class="single-meta-info col-md-12 <?php if (empty($post_meta_data['REAL_HOMES_property_balkon'][0])) echo "display-none" ?>">
							<?php
							if( !empty($post_meta_data['REAL_HOMES_property_balkon'][0]) ) {
							echo "<span>Balkon: </span>";
							$prop_balkon = $post_meta_data['REAL_HOMES_property_balkon'][0];
							echo $prop_balkon;
							} ?>
						 </div>
						 <div class="single-meta-info col-md-12 <?php if (empty($post_meta_data['REAL_HOMES_property_taras'][0])) echo "display-none" ?>">
						 	<?php
							if( !empty($post_meta_data['REAL_HOMES_property_taras'][0]) ) {
							echo "<span>Taras: </span>";
							$prop_taras = $post_meta_data['REAL_HOMES_property_taras'][0];
							echo $prop_taras;
							} ?>
						 </div>
					</div><!-- end row -->
				</div><!-- end column -->
				<div class="col-md-6 col-lg-7">
					<div class="row">
						<div class="single-meta-info col-md-12 <?php if (empty($post_meta_data['REAL_HOMES_property_komorka'][0])) echo "display-none" ?>">
						 	<?php
							if( !empty($post_meta_data['REAL_HOMES_property_komorka'][0]) ) {
							echo "<span>Komórka: </span>";
							$prop_komorka = $post_meta_data['REAL_HOMES_property_komorka'][0];
							echo $prop_komorka;
							} ?>
						 </div>
						  <div class="single-meta-info col-md-12 <?php if (empty($post_meta_data['REAL_HOMES_property_garage'][0])) echo "display-none" ?>">
							<?php
							if( !empty($post_meta_data['REAL_HOMES_property_garage'][0]) ) {
							echo "<span>Garaż: </span>";
							$prop_garage = $post_meta_data['REAL_HOMES_property_garage'][0];
							echo $prop_garage;
							} ?>
				 		</div>
						<div class="single-meta-info col-md-12 <?php $type_terms = get_the_terms($post -> ID, "property-rodzaj-budynku"); if (empty($type_terms)) echo "display-none"; ?>">
						 	<?php 
						 	$type_terms = get_the_terms($post -> ID, "property-rodzaj-budynku");
							$type_count = count($type_terms);
							if (!empty($type_terms)) {
								echo "<span>Budynek: </span>";
								$loop_count = 1;
								foreach ($type_terms as $typ_trm) {
									echo $typ_trm -> name;
									if ($loop_count < $type_count && $type_count > 1) {
										echo ', ';
									}
									$loop_count++;
								}
								echo '</span>';
							} else {
								echo '&nbsp;';
							}
							?>
				 		</div>
				 		<div class="single-meta-info col-md-12 <?php if (empty($post_meta_data['REAL_HOMES_property_pietro'][0])) echo "display-none" ?>">
						 	<?php
						if( !empty($post_meta_data['REAL_HOMES_property_pietro'][0]) ) {
						echo "<span>Piętro: </span>";
						$prop_pietro = $post_meta_data['REAL_HOMES_property_pietro'][0];
						echo $prop_pietro;
						} ?>
						 </div>
						 <div class="single-meta-info col-md-12 <?php if (empty($post_meta_data['REAL_HOMES_property_winda'][0])) echo "display-none" ?>">
						 	<?php
						if( !empty($post_meta_data['REAL_HOMES_property_winda'][0]) ) {
						echo "<span>Winda: </span>";
						$prop_winda = $post_meta_data['REAL_HOMES_property_winda'][0];
						echo $prop_winda;
						} ?>
						 </div>
						  
						  <div class="single-meta-info col-md-12 <?php if (empty($post_meta_data['REAL_HOMES_property_pow_dzialki'][0])) echo "display-none" ?>">
						 	<?php
						if( !empty($post_meta_data['REAL_HOMES_property_pow_dzialki'][0]) ) {
						echo "<span>Pow. działki: </span>";
						$pow_dzialki = $post_meta_data['REAL_HOMES_property_pow_dzialki'][0];
						echo $pow_dzialki;
						} ?>
						 </div>
						  <div class="single-meta-info col-md-12 <?php if (empty($post_meta_data['REAL_HOMES_property_czynsz'][0])) echo "display-none" ?>">
						 	<?php
						if( !empty($post_meta_data['REAL_HOMES_property_czynsz'][0]) ) {
						echo "<span>Czynsz: </span>";
						$prop_czynsz = number_format($post_meta_data['REAL_HOMES_property_czynsz'][0], 0, ' ', ' ');
						echo $prop_czynsz . " PLN";
						} 
						
						?>
						 </div>
						  <div class="single-meta-info col-md-12 <?php if (empty($post_meta_data['REAL_HOMES_property_przeznaczenie'][0])) echo "display-none" ?>">
						<?php if( !empty($post_meta_data['REAL_HOMES_property_przeznaczenie'][0]) ) {
						echo "<span>Przeznaczenie: </span>";
						$prop_przeznaczenie = $post_meta_data['REAL_HOMES_property_przeznaczenie'][0];
						echo $prop_przeznaczenie;
						} ?>
						 </div>
						 <div class="single-meta-info col-md-12 <?php if (empty($post_meta_data['REAL_HOMES_property_wlasnosc_gruntu'][0])) echo "display-none" ?>">
						 <?php
						if( !empty($post_meta_data['REAL_HOMES_property_wlasnosc_gruntu'][0]) ) {
						echo "<span>Własność gruntu: </span>";
						$prop_wlasnosc_gruntu = $post_meta_data['REAL_HOMES_property_wlasnosc_gruntu'][0];
						echo $prop_wlasnosc_gruntu;
						} ?>
						 </div>
						 <div class="single-meta-info col-md-12 <?php if (empty($post_meta_data['REAL_HOMES_property_media'][0])) echo "display-none" ?>">
						 	<?php
						if( !empty($post_meta_data['REAL_HOMES_property_media'][0]) ) {
						echo "<span>Media: </span>";
						$prop_media = $post_meta_data['REAL_HOMES_property_media'][0];
						echo $prop_media;
						} ?>
						 </div>
						 <div class="single-meta-info col-md-12 <?php if (empty($post_meta_data['REAL_HOMES_property_eksp_okienna'][0])) echo "display-none" ?>">
						 	<?php
						if( !empty($post_meta_data['REAL_HOMES_property_eksp_okienna'][0]) ) {
						echo "<span>Ekspozycja: </span>";
						$prop_eksp_okienna = $post_meta_data['REAL_HOMES_property_eksp_okienna'][0];
						echo $prop_eksp_okienna;
						} ?>
						 </div>
						<?php $panstwo = get_the_terms($post -> ID, "property-panstwo"); ?>

						<?php if($panstwo[0]->name == 'Polska' || $panstwo == false) { ?>
							<div class="single-meta-info col-md-12">
						 	<span>Cena/m<sup>2</sup> &nbsp; </span><?php property_price_za_metr(); ?>
						</div>
						<div class="single-meta-info col-md-12 cena">
						 	<span>Cena:</span><?php property_price(); ?>
						</div>
										
						<?php }else { ?>
										
						<div class="single-meta-info col-md-12">
							<span>Cena/m<sup>2</sup> &nbsp; </span><?php property_price_za_metr_eu(); ?>
						</div>
						<div class="single-meta-info col-md-12 cena">
						 	<span>Cena:</span><?php property_price_eu(); ?>
					    </div>

						<?php } ?> 

						 
						  
					</div><!-- end row -->
				</div><!-- end column -->
			</div><!-- end row -->				
		</div><!-- end column col-md-9-->
		<div class="col-md-3 meta-right-column">
				<div class="row">
					<div class="col-md-12">
						<?php if( get_field('plan_nieruchomości') ): ?>
						<div class="plany">
							<?php if( get_field('plan_nieruchomości') ): ?>
							  <a href="<?php the_field('plan_nieruchomości') ?>">
							      <i class="fa fa-image"></i><span>Plan</span>
							  </a>
							<?php endif; ?>
							<?php if( get_field('plan_nieruchomości_2') ): ?>
							  <a href="<?php the_field('plan_nieruchomości_2') ?>">
							      <i class="fa fa-image"></i><span>Plan</span>
							  </a>
							<?php endif; ?>
							<?php if( get_field('plan_nieruchomości_3') ): ?>
							  <a href="<?php the_field('plan_nieruchomości_3') ?>">
							      <i class="fa fa-image"></i><span>Plan</span>
							  </a>
							<?php endif; ?>
							<?php if( get_field('plan_nieruchomości_4') ): ?>
							  <a href="<?php the_field('plan_nieruchomości_4') ?>">
							      <i class="fa fa-image"></i><span>Plan</span>
							  </a>
							<?php endif; ?>
						</div>
						<?php endif; ?>
<span class="printer-icon"><a href="javascript:window.print()"><i class="fa fa-print"></i>Drukuj</a></span>
					</div>
				</div>
						 	<div class="row">
						 		<div class="col-md-12">
						 			<div class="numer-oferty">
						 				<?php $property_id = get_post_meta($post -> ID, 'REAL_HOMES_property_id', true);
						 			 					if (!empty($property_id)) {
						 			 					echo "Numer oferty: " . $property_id;
						 			 					}
						 				?>
						 			</div>
						 		</div>
						 	</div>
			</div>
	</div>
 </div>