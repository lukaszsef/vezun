��          �      �       0     1  O   A  {   �       	        '     *     2  	   9  �   C  �   �     m  U       �  o   �  �   U     �     �     �     �       
     �     o   �     R                
                            	              <!-- OG: %s --> Hey %s, you've been using %s for a while now, and we hope you're happy with it. If you like <strong>OG</strong> please leave us a %s&#9733;&#9733;&#9733;&#9733;&#9733;%s rating. A huge thanks in advance! Marcin Pietrzak No thanks OG Rate %s Saving Thanks :) Very tiny Open Graph plugin - add featured image as facebook image. This plugin do not have any configuration - you can check how it works looking into page source. We've spent countless hours developing this free plugin for you, and we would really appreciate it if you dropped us a quick rating! http://iworks.pl/ PO-Revision-Date: 2017-05-04 18:15:44+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: GlotPress/2.4.0-alpha
Language: pl
Project-Id-Version: Plugins - OG - Stable (latest release)
 <!-- OG: %s --> Hej %s, od jakiegoś czasu używasz wtyczki %s, i mamy nadzieję, że wszystko jest w jak najlepszym porządku. Jeżeli lubisz wtyczkę <strong>OG</strong>, proszę daj jej ocenę %s&#9733;&#9733;&#9733;&#9733;&#9733;%s. Z góry bardzo dziękuję! Marcin Pietrzak Nie dziękuję OG Oceń %s Zapisywanie Dzięki :) Bardzo mała wtyczka implementująca Open Grapha - Dodaje miniaturę wpisu jako obrazek dla FaceBooka. Wtyczka nie ma żadnej konfiguracji, aby sprawdzić czy działa, obejrzyj źródło strony. Spędziliśmy mnóstwo czasu tworząc tę darmową wtyczkę i bylibyśmy wdzięczni za pozytywne jej ocenienie! http://iworks.pl/ 