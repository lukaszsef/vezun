<?php
require_once('./wp-config.php' );
include_once("./wp-includes/wp-db.php");


$aktualizuj = aktualizujIlosci();

$custom_terms = get_terms('property-panstwo');


foreach($custom_terms as $custom_term) {

    $woj_mazowieckie = get_term_by('slug', 'mazowieckie', 'property-wojewodztwo');

    if($custom_term->slug == 'polska'){
        wp_reset_query();
        $args = array(
            'posts_per_page' => '-1',
            'post_status' => array('publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit', 'trash'),
            'post_type' => 'property',
            'tax_query' => array(
                array(
                    'taxonomy' => 'property-panstwo',
                    'field' => 'slug',
                    'terms' => 'polska',
                ),
            ),
        );

        $loop = new WP_Query($args);

        foreach($loop->posts as $single_post){
            add_term( $single_post->ID, $woj_mazowieckie->term_id, $term_order = 0 );
        }

    }
    
}

$wszystkie_nieruchomosci = get_posts([
    'post_type' => 'property',
    'post_status' => array('publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit', 'trash'),
    //'post_status' => 'publish',
    'numberposts' => -1
]);
//global $wp_taxonomies;
foreach($wszystkie_nieruchomosci as $nieruchomosc){
    $stary_agent_id = get_post_meta($nieruchomosc->ID, 'REAL_HOMES_agents',true);
    if($stary_agent_id != "-1"){
        $slug_stary_agent = get_post_field( 'post_name', $stary_agent_id );
        $nowy_agent = get_term_by('slug', $slug_stary_agent, 'property-doradca');

        //$nowy_agent->id;

        if($nowy_agent){
            add_term( $nieruchomosc->ID, $nowy_agent->term_id, $term_order = 0 );
        }
    }
    
}

function add_term($id, $agent, $term_order){

    global $wpdb;

    $query = "INSERT IGNORE INTO wp_term_relationships
    (`object_id`,`term_taxonomy_id`,`term_order`)
    VALUES ('$id', '$agent', '$term_order')";

    $results = $wpdb->get_results($query);
}

function aktualizujIlosci(){

    global $wpdb;

    $query = "select * from wp_term_taxonomy where taxonomy = 'property-doradca' OR taxonomy = 'property-wojewodztwo'";

    $taxonomie = $wpdb->get_results($query);

    foreach($taxonomie as $taxonomia){
        (int)$id_term = $taxonomia->term_taxonomy_id;
        $ilosc =  $wpdb->get_row("select count(term_taxonomy_id) as `count` from wp_term_relationships where term_taxonomy_id = '$id_term'");
        $ilosc_count = (int)$ilosc->count;
        $response = $wpdb->get_results("UPDATE wp_term_taxonomy SET `count` = '$ilosc_count' where term_taxonomy_id = '$id_term'");
    }

}

?>