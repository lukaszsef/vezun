<?php

/**
 * Podstawowa konfiguracja WordPressa.
 *
 * Ten plik zawiera konfiguracje: ustawień MySQL-a, prefiksu tabel
 * w bazie danych, tajnych kluczy i ABSPATH. Więcej informacji
 * znajduje się na stronie
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Kodeksu. Ustawienia MySQL-a możesz zdobyć
 * od administratora Twojego serwera.
 *
 * Ten plik jest używany przez skrypt automatycznie tworzący plik
 * wp-config.php podczas instalacji. Nie musisz korzystać z tego
 * skryptu, możesz po prostu skopiować ten plik, nazwać go
 * "wp-config.php" i wprowadzić do niego odpowiednie wartości.
 *
 * @package WordPress
 */

// ** Ustawienia MySQL-a - możesz uzyskać je od administratora Twojego serwera ** //
/** Nazwa bazy danych, której używać ma WordPress */
 //Added by WP-Cache Manager
define('DB_NAME', "webwave_vezun");

/** Nazwa użytkownika bazy danych MySQL */
define('DB_USER', "webwave_adm");

/** Hasło użytkownika bazy danych MySQL */
define('DB_PASSWORD', "LAJln0oI");

/** Nazwa hosta serwera MySQL */
define('DB_HOST', "185.208.164.151");

/** Kodowanie bazy danych używane do stworzenia tabel w bazie danych. */
define('DB_CHARSET', 'utf8');

/** Typ porównań w bazie danych. Nie zmieniaj tego ustawienia, jeśli masz jakieś wątpliwości. */
define('DB_COLLATE', '');

/**#@+
 * Unikatowe klucze uwierzytelniania i sole.
 *
 * Zmień każdy klucz tak, aby był inną, unikatową frazą!
 * Możesz wygenerować klucze przy pomocy {@link https://api.wordpress.org/secret-key/1.1/salt/ serwisu generującego tajne klucze witryny WordPress.org}
 * Klucze te mogą zostać zmienione w dowolnej chwili, aby uczynić nieważnymi wszelkie istniejące ciasteczka. Uczynienie tego zmusi wszystkich użytkowników do ponownego zalogowania się.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '+YYbQD^}@Hkae;Hons+z%-LeUuo&D+|&]u,%+oIB{kQtTsvIRat*/=,9l++ YU~?');
define('SECURE_AUTH_KEY',  'ugEY<PO0D2}D,dV9xSYpVf|.5OUBL&kN~d<-^*K~/]?la-SP!<lST:]gLlf04;B-');
define('LOGGED_IN_KEY',    '`+?m0Ak79Yy$X_8*OI0pB?@*~Q$$IpCn[A_s<a/INE.=to6SW]^6,Q*#yL0-qm`U');
define('NONCE_KEY',        '5L_S+-; v p4|Bk0QIfDo.>c|3)mGch=6:df{fBWKaHf[k_uLTe,m_Bp&%v$DO-+');
define('AUTH_SALT',        'OTyN=z]+a_d}m./-Eb_8k`QFO5 ^2o7aJkwZ78spA2,^70*<N6@=7SFLQ]I0gi2F');
define('SECURE_AUTH_SALT', 'D4Pfh=(O:N*Tj;2Wn_^#6^Wu47GeO:w`V!.P!O-EEyrvzqmLwAaos%^n#P_&1TIR');
define('LOGGED_IN_SALT',   'Y7wJ;(c=$=:1msljc9zgmd-I-DS&T3[GD_:i:0|7YaS.-Q>oTSSv~6q0QP`_@]j&');
define('NONCE_SALT',       '@OEm%lGgt2<yrQK.-~kauI.=Wthr%ogc<-+Ldl5Y9`>XE}qE8@Q+@-;!9FgQm;B2');

/**#@-*/

/**
 * Prefiks tabel WordPressa w bazie danych.
 *
 * Możesz posiadać kilka instalacji WordPressa w jednej bazie danych,
 * jeżeli nadasz każdej z nich unikalny prefiks.
 * Tylko cyfry, litery i znaki podkreślenia, proszę!
 */
$table_prefix  = 'wp_';
define('WP_MEMORY_LIMIT', '256M');
/**
 * Dla programistów: tryb debugowania WordPressa.
 *
 * Zmień wartość tej stałej na true, aby włączyć wyświetlanie ostrzeżeń
 * podczas modyfikowania kodu WordPressa.
 * Wielce zalecane jest, aby twórcy wtyczek oraz motywów używali
 * WP_DEBUG w miejscach pracy nad nimi.
 */
define('WP_DEBUG', false);

/* To wszystko, zakończ edycję w tym miejscu! Miłego blogowania! */

/** Absolutna ścieżka do katalogu WordPressa. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Ustawia zmienne WordPressa i dołączane pliki. */
require_once(ABSPATH . 'wp-settings.php');
